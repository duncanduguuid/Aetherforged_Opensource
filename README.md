We use slack to communicate internally about anything related to the AetherForged project. We request that you at least involve yourself in some discussion. You are not limited to your respective department and we welcome feedback on kits, lore, art and modelling. The announcement channel is only for announcements, chat or feedback on anything posted there should be done on another channel.

Discord is our public channel for community interaction. General chat is for chatting about anything (publicly viewable), Aetherforged talk is for talking about the game and also where we take questions during streams (public). Announcements are publically viewable but only developers can post there, mainly used for community-wide messages such as streams. Catslug chat is private for members of AetherForged, only we can see and post messages here. Private and Interview chats are private for their respective roles. Meme-regime is pretty self-explanatory (public).

Our main announcement method is via Reddit. If you pass along your Reddit username I'll get you a flair there. Reddit posts appear in #general on slack.

We hold a weekly meeting at 6pm UTC every Sunday, everyone is requested to join and discuss the latest changes in the project and bring up any concerns that have arisen in the last week of development. These will eventually be joined with a playtest session to help everyone understand the latest build and have a bit of fun competition. If you cannot attend the meeting let someone that is attending the meeting know and fill out the weekly document with your own progress. The backlog of meeting minutes are located in; AetherForged > Meetings > Management meeting

We ask that you type with common sense. We have a pretty open policy about sharing WIP assets but ask that you double check with Duncan "dink_dunk" Duguid or Eli "SaxPanther" Lord before posting screenshots or video footage. The same goes for dev-streams.

Please ensure if you are making documents in the google drive, in the sharing settings ensure that the owner is set to Duncan Duguid for security.

Below you can find a couple of handy resources that will aide in getting you up to speed on the project;

AetherForged design documentation;
https://docs.google.com/document/d/1_iWaBOX5z3Y581hJW7CCWqMXbFdbA2ffh29WhBRyj1c/edit?usp=sharing

Design Choices in AetherForged;
https://docs.google.com/document/d/1-wclah3qUGrffHkoko_9Tnh4-q-7LdheNEccYLa1GxQ/edit?usp=sharing

Lore 101;
https://docs.google.com/document/d/11eWhTllcDRh4qBKMUeTovB3Fl32zRToNs9u6HRPdSYM/edit

Current development team;
https://docs.google.com/document/d/1pdjWG-JsaaloPLHWfvo1jqNecwKnHi2Wq5NePGbuGt4/edit?usp=sharing

How to install the project in unity;
https://docs.google.com/document/d/18ZVNzyau3d9XzrBq_pWn-AMDvWcXHRpIjD0BBm3q8aI/edit?usp=sharing


Disclaimer: We are looking for members that contribute to the project on a regular basis. If at any point we feel that your contributions are lacking/non-existent then it may result in having privileges removed until further contributions are made. Prolonged periods may result in being removed completely from the team and reapplication will be necessary. For periods where you are not able to reach a computer (e.g Holidays, Family reasons) we ask that you get in touch with your lead to ensure they know about it. Consider your first month of working on this project as an assessment period.
