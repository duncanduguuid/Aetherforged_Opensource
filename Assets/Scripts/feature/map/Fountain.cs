﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;


using core;
using core.utility;
using managers;

public class Fountain : Entity {

    public float percentPartialHeal = 5;

    public float percentStrongHeal = 20;

    public float percentZapDamage = 35;

    public float healAmount;

    public float deeeeeathDamage = 2000;



	// Use this for initialization
	void Start () {
        radius = 600f;
	}
	
	// Update is called once per frame
	void Update () {
		
        var enemyForgers = Unit.ListOf.Where( unit => {
            return unit.Team != Team && Utils.CollidesWith(unit.Position, unit.Radius, Position, Radius);
        });

        var allyForgers = UnitManager.GetForgersOnTeam(Team).Where( unit => {
            return Utils.CollidesWith(unit.Position, unit.Radius, Position, Radius);
        });

        foreach (Unit ally in allyForgers) {
            if (ally.InCombat)
            {
                healAmount = ally.MaxHealth * percentPartialHeal.Percent() * Time.deltaTime;
            }
            else
            {
                healAmount = ally.MaxHealth * percentStrongHeal.Percent() * Time.deltaTime;
            }
            ProvideHeal(ally, new HealPacket(HealActionType.HealBurst, this, true) { HealAmount = healAmount });
        }

        foreach (Unit enemy in enemyForgers) {
            var damageAmount = enemy.MaxHealth * percentZapDamage.Percent() * Time.deltaTime;
            DealDamage(enemy, new DamagePacket(this, DamageActionType.SpellCast) { PureDamage = damageAmount });
        }


	}


    #if UNITY_EDITOR
    void OnDrawGizmosSelected () {
        // Display the radius when selected
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, Radius * Constants.TO_UNITY_UNITS);

    }
    #endif

}
