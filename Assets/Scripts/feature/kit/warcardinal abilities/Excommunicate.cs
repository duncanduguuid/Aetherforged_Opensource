﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Excommunicate", menuName = "Ability/Warcardinal/Excommunicate")]
    public class Excommunicate : Castable {


        #region excommunicate variables
        //excommunicate base variables
        public float[] excommunicateCooldown = new float[] { 10, 9, 8, 7, 6 };
        public float excommunicateRange = 525;
        //excommunicate projectile variables
        public float excommunicateMissileSpeed = 1000;
        public string excommunicateProjectile = "excommunicate_projectile";
        //excommunicate damage variables
        public float[] excommunicateBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float excommunicateDamagePowerScaling = .5f;
        //excommunicate empower variables
        public bool excommunicateIsEmpowered = false;
        //excommunicate debuff variables
        public static BuffDebuff excommunicateDebuff;
        public static BuffDebuff excommunicateEmpoweredDebuff;
        public float excommunicateDebuffDuration = 2.5f;
        //excommunicate debuff CC variables
        public float[] excommunicateBaseSlowAmount = new float[] { .25f, .3f, .35f, .4f, .45f };
        public float excommmunicateSlowPowerScaling = .05f;
        public float excommunicateExtraSlowAmount = .20f;
        #endregion

        #region first active
        public Excommunicate()
        {
            Name = "excommunicate";
            Cooldowns = excommunicateCooldown; //need to update this with a scaling cooldown
            GetIconSubtext = (ability, caster) =>
            {
                return 10.ToString();
            };
            TargetedCastingConditions = Ability.UnitTargetShortcuts.EnemySimple;
            UnitTargetedCast = (ability, caster, target) =>
            {

                float baseDamage = excommunicateBaseDamage[caster.GetAbilityLevelLessOne(ability)];
                float totalDamage = baseDamage + (excommunicateDamagePowerScaling * caster.Power);
                caster.PlayAnimation("Ability1");
                var excommunicateMissile = ProjectileManager.Instance.Gimme(excommunicateProjectile, target, caster, excommunicateMissileSpeed);
                excommunicateMissile.OnHitTarget = (excommMissile, source, hitUnit) =>
                {
                    if (hitUnit == target)
                    {
                        caster.DealDamage(hitUnit, new DamagePacket
                        {
                            MagicalDamage = totalDamage,
                        });

                        //create the logic for applying slows here
                        target.AddBuff(excommunicateDebuff);
                        if (caster.Resource.Amount >= caster.Resource.MaxAmount)
                        {
                            caster.Resource.Amount -= caster.Resource.Amount;
                            target.AddBuff(excommunicateEmpoweredDebuff);
                        }
                        excommMissile.BreakMe();
                    }
                };
            };
        }

        #endregion
    }
}