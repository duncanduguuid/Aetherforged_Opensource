﻿using core;
using core.forger;

namespace feature.kit
{
    public class Tank : Obsolete_Archetype
    {
        public override UnitStats BaseStats
        {
            get
            {
                return UnitStats.Stats(
                    MaxHealth: 530,
                    HealthRegen: 1.6f,
                    MovementSpeed: 385,
                    AutoAttackDamage: 61,
                    BaseAttackTime: 1.5f,
                    BaseAttackRange: Constants.MELEE_DEFAULT_ATTACK_RANGE
                );
            }
        }

        public override UnitStats PerLevelStats
        {
            get
            {
                // Note: Right now (level - 1) is applied from per level stats. 
                // E.g. At level one, they get BaseStats only. At level two, they get BaseStats + 1 x PerLevelStats. 
                // And At level six, they get BaseStats + 5 x PerLevelStats.

                return UnitStats.Stats(
                    MaxHealth: 78,
                    HealthRegen: .11f,
                    AutoAttackDamage: 2.85f,
                    AttackSpeed: 2.4f
                );
            }
        }

        public override float basicAttackDamagePerPower
        {
            get
            {
                return .8f;
            }
        }

        public override float attackSpeedPerHaste
        {
            get
            {
                return .5f;
            }
        }

        public override float cooldownReductionPerHaste
        {
            get
            {
                return .5f;
            }
        }

    }
}

