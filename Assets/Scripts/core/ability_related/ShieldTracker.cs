﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;
using core.collection;

public class ShieldTracker {

    Dictionary<SourcePair, ShieldData> dataTable = new Dictionary<SourcePair, ShieldData>();

    MinHeap<float, SourcePair> magicShields = new MinHeap<float, SourcePair>();
    MinHeap<float, SourcePair> physicShields = new MinHeap<float, SourcePair>();
    MinHeap<float, SourcePair> generalShields = new MinHeap<float, SourcePair>();

    protected Unit holder;

    protected float generalShieldAmount;

    protected float magicalShield;

    protected float physicalShield;

    public ShieldTracker(Unit myUnit) 
    {
        this.holder = myUnit;
    }

    public float Value {
        get {
            return generalShieldAmount;
        }
    }

    public void Add(SourcePair pair) {
        var buff = pair.ability as BuffDebuff;

        if (buff is ShieldBuff && buff.Duration > 0) {
            float timeout = GameTime.time + buff.Duration;
            ShieldBuff shield = pair.ability as ShieldBuff;

            ShieldData data;
            float delta;

            if (dataTable.ContainsKey(pair))
            {
                data = dataTable[pair];

                var adjustedAmount = Mathf.MoveTowards(data.Amount, shield.Maximum(pair), shield.Amount(pair));

                if (adjustedAmount == data.Amount)
                    return;
                
                delta = adjustedAmount - data.Amount;
                dataTable[pair].Amount += delta;
            }
            else
            {
                data = new ShieldData { Amount = shield.Amount(pair), Magical = shield.Magical, Physical = shield.Physical };
                delta = data.Amount;
                dataTable.Add(pair, data);
            }

            if (data.Magical && data.Physical)
            {
                generalShieldAmount += delta;
                generalShields.Add(timeout, pair);
            }
            else if (data.Magical)
            {
                magicalShield += delta;
                magicShields.Add(timeout, pair);
            }
            else if (data.Physical)
            {
                physicalShield += delta;
                physicShields.Add(timeout, pair);
            }
        }
    }

    public float MagicalImpact(float amount) {
        while (amount > 0 && magicalShield > 0)
        {
            var topBit = magicShields.Top();
            float shieldAmount = dataTable[topBit].Amount;
            if (amount >= shieldAmount)
            {
                amount -= shieldAmount;
                holder.RemoveBuff(topBit);
            }
            else
            {
                dataTable[topBit].Amount -= amount;
                magicalShield -= amount;
                amount = 0;
            }
        }

        return GeneralImpact(amount);
    }

    public float PhysicalImpact(float amount) {
        while (dataTable.Count > 0 && amount > 0 && physicalShield > 0)
        {
            var topBit = magicShields.Top();
            float shieldAmount = dataTable[topBit].Amount;
            if (amount >= shieldAmount)
            {
                amount -= shieldAmount;
                holder.RemoveBuff(topBit);
            }
            else
            {
                dataTable[topBit].Amount -= amount;
                physicalShield -= amount;
                amount = 0;
            }
        }

        // run it through the general soak before returning.
        return GeneralImpact(amount);
    }

    public float GeneralImpact(float amount) {
        while (dataTable.Count > 0 && amount > 0 && generalShieldAmount > 0)
        {
            var topBit = generalShields.Top();
            float shieldAmount = dataTable[topBit].Amount;
            if (amount >= shieldAmount)
            {
                amount -= shieldAmount;
                Remove(topBit);
                holder.RemoveBuff(topBit);
            }
            else
            {
                dataTable[topBit].Amount -= amount;
                generalShieldAmount -= amount;
                amount = 0;
            }
        }


        return amount;
    }

    public void Remove(SourcePair pair) {
        if (pair.ability is ShieldBuff && dataTable.ContainsKey(pair))
        {
            var data = dataTable[pair];
            if (data.Magical && data.Physical)
            {
                generalShieldAmount = Mathf.MoveTowards(generalShieldAmount, 0, data.Amount);
                generalShields.Remove(pair);
            }
            else if (data.Magical)
            {
                magicalShield = Mathf.MoveTowards(magicalShield, 0, data.Amount);
                magicShields.Remove(pair);
            }
            else if (data.Physical)
            {
                physicalShield = Mathf.MoveTowards(physicalShield, 0, data.Amount);
                physicShields.Remove(pair);
            }

            dataTable.Remove(pair);
        }
    }
}
