﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPopupDescriptions : Singleton<UIPopupDescriptions> {

	private Dictionary<string, string> uiPopupDictionary = new Dictionary<string, string>();

	void Start(){
		uiPopupDictionary.Add("Power", "This stat increases how much damage, both basic attacks and abilities, will do. It also increases archetype specific mechanics, such as shields, buffs and debuffs, heals, and CC durations. Scalings with abilities are ability specific. Scaling with basic attacks is archetype specific.");
		//a
	}

	public string PopupText (string PopupName) {
		if(uiPopupDictionary.ContainsKey(PopupName))
		{
			return uiPopupDictionary[PopupName];
		}
		return string.Empty;
	}
}
