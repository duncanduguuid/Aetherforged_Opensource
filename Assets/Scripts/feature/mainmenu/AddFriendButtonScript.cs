﻿using core.http;
using feature.mainmenu.models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class AddFriendButtonScript : MonoBehaviour {

    public Uri CreateFriendRequestEndpoint { get { return new Uri(NetworkStore.BaseUrl + "/api/Friend/CreateFriendRequest"); } }

    public GameObject UsernameInputField;
    public GameObject SocialSidepanel;

	// Use this for initialization
	void Start () {
        UsernameInputField.SetActive(false);
	}

    public GameObject ErrorPanel;
    private MainMenuErrorScript ErrorLogging {
        get {
            return ErrorPanel.GetComponent<MainMenuErrorScript>();
        }
    }

    public void OnPress() {
        if(UsernameInputField.activeInHierarchy) {
            UsernameInputField.SetActive(false);
            UsernameInputField.GetComponent<InputField>().text = "";
        } else {
            UsernameInputField.SetActive(true);
        }
    }

    public void AddFriend() {
        var httppost = new HttpPost() {
            Endpoint = CreateFriendRequestEndpoint,
            JsonModel = new FriendRequestModel() {
                StringIdentifier = NetworkStore.SessionIdentifer,
                Username = UsernameInputField.GetComponent<InputField>().text,
            }
        };
        StartCoroutine(httppost.CoSend((response) => {
            if (response.Status == HttpStatusCode.OK) {
                SocialSidepanel.GetComponent<FriendslistScript>().isDirty = true;
            } else {
                ErrorLogging.Throw(response);
            }
        }));
    }
}
