﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using core;
using feature;
using controllers;

public class AnnouncerController : Singleton<AnnouncerController> {

	public List<AnnouncerEvent> EventQueue = new List<AnnouncerEvent>();
	public List<AnnouncerEvent> KillFeed = new List<AnnouncerEvent>();
	public List<AnnouncerEvent> Alerts = new List<AnnouncerEvent>();

	public GameObject Canvas {
		get;
		private set;
	}
	public GameObject AnnouncementPanel {
		get;
		private set;
	}
	public GameObject KillFeedPanel {
		get;
		private set;
	}

	public Text AnnouncementText {
		get;
		private set;
	}

	public Image AnnouncementBar
	{
		get;
		private set;
	}

	private Vector3 Pos;

	private Image ActorRing;
	private Image ActorPortrait;
	private Image RecipientRing;
	private Image RecipientPortrait;
	private Image OmniFrame;
	private Image OmniPortrait;

	private bool isQueueEmpty = true;
	private bool isAlertsEmpty = true;
	private bool isKillFeedEmpty = true;


	// Use this for initialization
	void Awake () {
		if(Instance != null)
		{
			Debug.LogError("Only one announcer controller should be present at a time");
		}
		else
		{
			instance = this;
		}
		

	}

	
	// private void PopQueue()
	// {
	// 	if(isQueueEmpty)
	// 	{
	// 		return;
	// 	}
		
	// 	AnnouncerEvent currEvent = EventQueue[0];
		
	// 	CancelInvoke();
	// 	currEvent.PlayEvent(currEvent, AnnouncerSource.Announcement);
		
	// 	EventQueue.RemoveAt(0);
	// 	if(EventQueue.Count < 1)
	// 	{
	// 		if(!isQueueEmpty)
	// 		{
	// 			isQueueEmpty = true;
	// 		}
	// 	}
	// 	InvokeRepeating("PopAlert", 5f, .5f);
	// 	InvokeRepeating("PopQueue", 5f, .5f);
	// 	InvokeRepeating("PopKillFeed", .2f, .5f);
	// 	Invoke("ClearAnnouncements", 4f);
	// }

	// private void PopAlert()
	// {
	// 	if(isAlertsEmpty)
	// 	{
	// 		return;
	// 	}

	// 	AnnouncerEvent currEvent = Alerts[0];

	// 	CancelInvoke();
	// 	currEvent.PlayEvent(currEvent, AnnouncerSource.Alert);

	// 	Alerts.RemoveAt(0);
	// 	if(Alerts.Count < 1)
	// 	{
	// 		if(!isAlertsEmpty)
	// 		{
	// 			isAlertsEmpty = true;
	// 		}
	// 	}
	// 	InvokeRepeating("PopAlert", .5f, .5f);
	// 	InvokeRepeating("PopQueue", .5f, .5f);
	// 	InvokeRepeating("PopKillFeed", .2f, .5f);
	// }

	// private void PopKillFeed()
	// {
	// 	if(isKillFeedEmpty)
	// 	{
	// 		return;
	// 	}

	// 	AnnouncerEvent currEvent = KillFeed[0];

	// 	CancelInvoke();
	// 	currEvent.PlayEvent(currEvent, AnnouncerSource.KillFeed);

	// 	KillFeed.RemoveAt(0);
	// 	if(KillFeed.Count < 1)
	// 	{
	// 		if(!isKillFeedEmpty)
	// 		{
	// 			isKillFeedEmpty = true;
	// 		}
	// 	}
	// 	InvokeRepeating("PopAlert", 0f, .5f);
	// 	InvokeRepeating("PopQueue", 0f, .5f);
	// 	InvokeRepeating("PopKillFeed", 0f, .5f); 
	// }

	
	void Start()
	{
		InvokeRepeating("PopQueue", 0f, .5f);
		InvokeRepeating("PopAlert", 0f, .5f);
		InvokeRepeating("PopKillFeed", 0f, .5f);
		Canvas = GameObject.Find("HudCanvas");
		AnnouncementPanel = Canvas.transform.Find("AnnouncementPanel").gameObject;
		KillFeedPanel = Canvas.transform.Find("KillFeedPanel").gameObject;
		AnnouncementText = GameObject.Find("AnnouncementText").GetComponent<Text>();
		AnnouncementBar = GameObject.Find("AnnouncementBar").GetComponent<Image>();
		ActorRing = GameObject.Find("ActorRing").GetComponent<Image>();
		RecipientRing = GameObject.Find("RecipientRing").GetComponent<Image>();
		OmniFrame = GameObject.Find("OmniFrame").GetComponent<Image>();
		ActorPortrait = GameObject.Find("ActorPortrait").GetComponent<Image>();
		RecipientPortrait = GameObject.Find("RecipientPortrait").GetComponent<Image>();
		OmniPortrait = GameObject.Find("OmniPortrait").GetComponent<Image>();

		Pos = AnnouncementPanel.transform.position;
	}
	public void QueueEvent(AnnouncerEvent currEvent)
	{
		EventQueue.Add(currEvent);
		Debug.Log("Announcement Queued");
		if(isQueueEmpty)
		{
			isQueueEmpty = false;
		}
	}

	public void QueueAlert(AnnouncerEvent currEvent)
	{
		Alerts.Add(currEvent);
		if(isAlertsEmpty)
		{
			isAlertsEmpty = false;
		}
	}

	public void QueueKillFeed(AnnouncerEvent currEvent)
	{
		KillFeed.Add(currEvent);
		if(isKillFeedEmpty)
		{
			isKillFeedEmpty = false;
		}
	}

	public void ClearAnnouncements()
	{
		AnnouncementText.CrossFadeAlpha(0f, .1f, false);
		AnnouncementBar.CrossFadeAlpha(0f, .1f, false);
		ActorRing.CrossFadeAlpha(0f, .1f, false);
		RecipientRing.CrossFadeAlpha(0f, .1f, false);
		OmniFrame.CrossFadeAlpha(0f, .1f, false);
		ActorPortrait.CrossFadeAlpha(0f, .1f, false);
		RecipientPortrait.CrossFadeAlpha(0f, .1f, false);
		AnnouncementText.text = "";
		if(AnnouncementPanel.transform.position != Pos)
		{
			AnnouncementPanel.transform.position = Pos;
		}
	}
	// Update is called once per frame
	void FixedUpdate () {
	}
}
