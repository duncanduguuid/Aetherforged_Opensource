﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Avalanche", menuName = "Ability/Ember/Avalanche")]
	public class Avalanche : Castable
	{
		[SerializeField] float[] damage;
		[SerializeField] float powerScaling;
		[SerializeField] float windUp;
		[SerializeField] float range;
		[SerializeField] float width;
		[SerializeField] float speed;
		[SerializeField] float coneAngle;
		[SerializeField] float coneLength;
		[SerializeField] BuffDebuff slow;
		private float turnDuration;

		public Avalanche()
		{
			LineTargetCast = (ability, caster, location) =>
			{
				Vector2 ogPosition = caster.Position;
				float turnDuration = 1;
				Vector2 targetLocation = Utils.PointWithinRange(caster.Position, location, range, range);
				
				caster.PlayAnimation("Ability3.Start");
				caster.HaltAndFaceTarget(targetLocation, turnDuration);
				
				new Timer().Start(() => 
				{
					caster.PlayAnimation("Ability3");
					caster.Dash(ability, targetLocation, speed, onStop: (dash) =>
					{
						caster.PlayAnimation("Ability3.End");

						float totalDamage = damage[caster.GetAbilityLevelLessOne(ability)] + powerScaling * caster.Power;
						Vector2 normEndPoint = caster.Position + (caster.Position - ogPosition).normalized;
						
						Unit.ForEachEnemyInCone(coneAngle, coneLength, caster.Position, normEndPoint, caster.Team, (hitUnit) =>
						{
							caster.DealDamage(hitUnit, new DamagePacket(DamageActionType.SkillOrAbility, caster)
								{PhysicalDamage = totalDamage});
							hitUnit.AddBuff(slow.WithSource(caster));
						});
						dash.BreakMe();
					});
					return 0f;
				},windUp);
				caster.StartCooldown(ability);
			};
		}
	}
}