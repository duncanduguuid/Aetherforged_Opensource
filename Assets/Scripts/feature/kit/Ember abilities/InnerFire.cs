﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Inner Fire", menuName = "Ability/Ember/Inner Fire")]
	public class InnerFire : Passive
	{
		[SerializeField] float resourceGain;
		[SerializeField] float healthThreshold;
		[SerializeField] float resourceDecay;
		
		private bool inCombat;
		private float count;
		public InnerFire()
		{
			OnApply = (pair, holder) =>
			{
				inCombat = false;
				var resource = holder.Resource;
				holder.Resource -= resource.Amount;
			};
			
			OnBasicAttack = (pair, holder, target, packet) =>
			{
				//if(target != packet.PrimaryTarget || holder.HasBuff(coldSteelBuff.WithSource(holder)))
				if(target.Kind == UnitKind.Tower || holder.Resource >= holder.Resource.MaxAmount)
				{
				}
				else if(target.HPRatio <= healthThreshold)
				{
					holder.Resource += (resourceGain * 2);
				}
				else
				{
					holder.Resource += resourceGain;
				}
				return packet;
			};
			OnEnterCombat = (pair, holder, data) =>
			{
				inCombat = true;
			};
			OnExitCombat = (pair, holder, data) =>
			{
				inCombat = false;
			};
			OnTick = (pair, holder, data, time) =>
			{
				count += time;
				
				if(inCombat == false && count > 1)
				{
					holder.Resource -= resourceDecay;
					count = 0;
				}
			};
			OnDeath = (pair, holder, data, killer) =>
			{
				holder.Resource.Amount = 0;
			};
		}
	}
}