using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using controllers;
using System.Collections.Generic;
using managers;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Warcardinal Imprint", menuName = "Units/Warcardinal Imprint")]
    public class ShieldKit : Imprint
    {
        /*
        public override Obsolete_Archetype obsolete_archetype
        {
            get
            {
                return new MeleeSupport();
            }
        }
        public override ForgerResource characterResource()
        {
            return new ForgerResource()
            {
                Name = "conviction",
                MinAmount = 0,
                MaxAmount = 5,
                Amount = 0,
                Color = new Color(1, 0, 1, .50f),
            };
        }
        */
        #region conviction variables
        //conviction base variables
        public Unit convictionPreviousUnit = null;
        //conviction debuff variables
        public static BuffDebuff convictionDebuff;
        public float convictionDebuffDuration = 5;
        public float convictionStunDuration = .65f;
        //conviction debuff immunity variables
        public static BuffDebuff convictionImmunityBuff;
        public float convictionImmunityDuration = 8;
        //conviction resource gain variables
        public float convictionResourceGain = 1;
        public float convictionEmpoweredResourceGain = 2;
        #endregion

        #region excommunicate variables
        //excommunicate base variables
        public float[] excommunicateCooldown = new float[] { 10, 9, 8, 7, 6 };
        public float excommunicateRange = 525;
        //excommunicate projectile variables
        public float excommunicateMissileSpeed = 1000;
        public string excommunicateProjectile = "excommunicate_projectile";
        //excommunicate damage variables
        public float[] excommunicateBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float excommunicateDamagePowerScaling = .5f;
        //excommunicate empower variables
        public bool excommunicateIsEmpowered = false;
        //excommunicate debuff variables
        public static BuffDebuff excommunicateDebuff;
        public static BuffDebuff excommunicateEmpoweredDebuff;
        public float excommunicateDebuffDuration = 2.5f;
        //excommunicate debuff CC variables
        public float[] excommunicateBaseSlowAmount = new float[] { .25f, .3f, .35f, .4f, .45f };
        public float excommmunicateSlowPowerScaling = .05f;
        public float excommunicateExtraSlowAmount = .20f;
        #endregion

        #region cardinal call variables
        //cardinal call base variables
        public float[] cardinalCallCooldown = new float[] { 1, 1, 1, 1, 1 };
        public int cardinalCallAoERadius = 325;
        public int cardinalCallBaseRangeIncrease = 100;
        public float cardinalCallShieldRangePowerScaling = .25f;
        //cardinal call shield variables
        public float cardinalCallShieldDuration = 3;
        public float[] cardinalCallBaseShield = new float[] { 1, 1, 1, 1, 1 };
        public float cardinalCallPowerScaling = .5f;
        public float cardinalCallShieldReductionPercentage = .2f;
        public float[] cardinalCallMinimumShieldEffect = new float[] { .2f, .3f, .4f, .5f, .6f };
        //cardinal call empowered variables
        public bool cardinalCallIsEmpowered = false;
        #endregion

        #region sinbrand variables
        //sinbrand base variables
        public float[] sinbrandCooldown = new float[] { 1, 1, 1, 1, 1 };
        public float sinbrandRange = 450;
        //sinbrand damage variables
        public float[] sinbrandBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float sinbrandPowerScaling = .5f;
        //sinbrand empowered variables
        public bool sinbrandIsEmpowered = false;
        //sinbrand debuff variables
        public BuffDebuff sinbrandDebuff;
        public float sinbrandDebuffDuration = 2;
        public float[] sinbrandPercentDamageIncrease= new float[] { .04f, .05f, .06f, .07f, .08f };
        //sinbrand empowered debuff
        public BuffDebuff sinbrandEmpoweredDebuff;
        public float sinbrandEmpoweredDebuffDuration = 2;
        //sinbrand buff variables
        public float sinbrandEmpoweredBoost = 30;
        public float sinbrandMoveBoostDuration = .5f;
        #endregion

        #region fist of ouros variables
        //fist of ouros base variables
        public float[] fistofOurosCooldown = new float[] { 1, 1, 1 };
        public float fistofOurosRange = 375;
        public int fistofOurosWidth = 250;
        //fist of ouros damage variables
        public float[] fistofOurosBaseDamage = new float[] { 1, 1, 1 };
        public float fistofOurosPowerScaling = .5f;
        //fist of ouros cc variables
        public int fistofOurosKnockbackDistance = 150;
        public int fistofOurosKnockbackHeight = 100;
        public float fistofOurosKnockbackDuration = .5f;
        //fist of ouros debuff variables
        public BuffDebuff fistofOurosDebuff;
        public float fistofOurosStunDuration = 1.5f;
        #endregion
        /*
        public override string Name { get { return "lucan"; } }

        public override float AttackHitTime
        {
            get
            {
                return 0.12f;
            }
        }
        */
        protected override void Initialize(Unit me)
        {
            #region passive ability
            convictionDebuff = new BuffDebuff {
                Name = "conviction_debuff",
                MaximumStacks = 3,
                Obsolete_Duration = (self) => {
                    return convictionDebuffDuration;
                },
                //do thing when stack is added - when 3 stacks are added, stun the unit
            };

            convictionImmunityBuff = new BuffDebuff()
            {
                Name = "conviction_immunity"
            };

            var convictionPassive = new Passive()
            {
                Name = "conviction",
                OnBasicAttackStart = (SourcePair pair, Unit holder, Unit target) =>
                {
                    if (target is Forger /*|| (target is Monster && (target as Monster).isLarge)*/)
                    { //to implement
                        holder.Resource += convictionEmpoweredResourceGain;
                        if (!target.HasBuff(convictionImmunityBuff.WithSource(holder)))
                        {
                            if (target != convictionPreviousUnit)
                            {
                                convictionPreviousUnit.RemoveBuff(convictionDebuff.WithSource(holder));
                            }
                            if (!target.HasBuff(convictionImmunityBuff.WithSource(holder)))
                            {
                                target.AddBuff(convictionDebuff.WithSource(holder));
                            }
                            convictionPreviousUnit = target;
                        }
                    }
                    else
                    {
                        holder.Resource += convictionResourceGain;
                    }
                }
            };
            Passives.Add(0, convictionPassive);
            #endregion

            #region first active
            var excommunicateActive = new Castable()
            {
                Name = "excommunicate",
                Cooldowns = excommunicateCooldown, //need to update this with a scaling cooldown
                GetIconSubtext = (ability, caster) => {
                    return 10.ToString();
                },
                TargetedCastingConditions = Ability.UnitTargetShortcuts.EnemySimple,
                UnitTargetedCast = (ability, caster, target) => {

                    float baseDamage = excommunicateBaseDamage[ability.Level - 1];
                    float totalDamage = baseDamage + (excommunicateDamagePowerScaling * caster.Power);
                    caster.PlayAnimation("Ability1");
                    var excommunicateMissile = ProjectileManager.Instance.Gimme(excommunicateProjectile, target, caster, excommunicateMissileSpeed);
                    excommunicateMissile.OnHitTarget = (excommMissile, source, hitUnit) => {
                        if (hitUnit == target)
                        {
                            caster.DealDamage(hitUnit, new DamagePacket
                            {
                                MagicalDamage = totalDamage,
                            });

                            //create the logic for applying slows here
                            target.AddBuff(excommunicateDebuff);
                            if (caster.Resource.Amount >= caster.Resource.MaxAmount)
                            {
                                caster.Resource.Amount -= caster.Resource.Amount;
                                target.AddBuff(excommunicateEmpoweredDebuff);
                            }
                            excommMissile.BreakMe();
                        }
                    };
                },
            };
            Skills.Add(1, excommunicateActive);
            #endregion

            #region second active
            var cardinalCallActive = new Castable()
            {
                Name = "cardinal_call",
                Cooldowns = cardinalCallCooldown,
                OnNonTargetedCast = (ability, caster) => {
                    float numAllies = 0;
                    List<Unit> alliesEffected = new List<Unit>();
                    alliesEffected.Add(caster);
                    int finalRange = cardinalCallAoERadius;
                    float rawShieldEfficency;
                    float minShieldEfficiency = cardinalCallMinimumShieldEffect[ability.Level - 1];
                    float currentShieldEffect;
                    float finalShieldValue;
                    caster.PlayAnimation("Ability2");
                    int bonusRange = (int)(cardinalCallShieldRangePowerScaling * caster.Power);

                    if (caster.Resource.Amount >= caster.Resource.MaxAmount)
                    {
                        cardinalCallIsEmpowered = true;
                        caster.Resource.Amount -= caster.Resource.Amount;
                    }

                    if (cardinalCallIsEmpowered)
                    {
                        finalRange += cardinalCallBaseRangeIncrease + bonusRange;
                    }
                    ForAlliesInRadius(finalRange, caster, (Unit ally) => {
                        if (ally is Forger && ally != caster)
                        {
                            numAllies++;
                            alliesEffected.Add(ally);
                        }
                    });

                    alliesEffected.ToArray();
                    rawShieldEfficency = 1 - (numAllies * cardinalCallShieldReductionPercentage);
                    if (cardinalCallIsEmpowered)
                    {
                        rawShieldEfficency = 1;
                    }
                    currentShieldEffect = (rawShieldEfficency >= minShieldEfficiency) ? rawShieldEfficency : minShieldEfficiency;
                    finalShieldValue = (cardinalCallBaseShield[ability.Level - 1] + (cardinalCallPowerScaling * caster.Power)) * currentShieldEffect;

                    ShieldBuff cardinalShield = new ShieldBuff() {
                        Name = ability.Name+"_shield",
                        Parent = ability,
                        Amount = (self) => {
                            return finalShieldValue;
                        },
                        Maximum = (self) => {
                            return finalShieldValue;
                        },
                        AlsoObsolete_Duration = (self) => {
                            return cardinalCallShieldDuration;
                        }
                    };

                    foreach (Unit target in alliesEffected)
                    {
                        target.AddShield(cardinalShield.WithSource(caster));
                    }
                    caster.StartCooldown(ability);
                }
            };
            Skills.Add(2, cardinalCallActive);
            #endregion

            #region third active
            //need to separate the base debuff from the empowered debuff
            sinbrandDebuff = new BuffDebuff() {
                Name = "heretic",
                Obsolete_OnTakeDamagePostMit = (Passive self, Unit attackSource, Unit debuffedUnit, DamagePacket packet) => {
                    //calculate multiplying the additional damage
                    return packet;
                },
                Obsolete_Duration = (Passive self) => {
                    return sinbrandDebuffDuration;
                }
            };

            sinbrandEmpoweredDebuff = new BuffDebuff() {
                Name = "zealot_debuff",
                Obsolete_OnTakeDamagePostMit = (Passive self, Unit attackSource, Unit debuffedUnit, DamagePacket packet) => {
                    //Add move speed buff to ally
                    //attackSource.AddBuff();
                    return packet;
                },
                Obsolete_Duration = (Passive self) => {
                    return sinbrandEmpoweredDebuffDuration;
                }
            };
            var sinbrandActive = new Castable()
            {
                Name = "sinbrand",
                Cooldowns = sinbrandCooldown,
                TargetedCastingConditions = Ability.UnitTargetShortcuts.EnemySimple,
                UnitTargetedCast = (ability, caster, target) => {
                    float baseDamage = sinbrandBaseDamage[ability.Level - 1];
                    float totalDamage = baseDamage + (sinbrandPowerScaling * caster.Power);
                    caster.PlayAnimation("Ability3");
                    caster.DealDamage(target, new DamagePacket() { MagicalDamage = totalDamage });
                    target.AddBuff(sinbrandDebuff);
                    if(caster.Resource.Amount >= caster.Resource.MaxAmount) {
                        caster.Resource.Amount -= caster.Resource.Amount;
                        target.AddBuff(sinbrandEmpoweredDebuff);
                    }
                }
            };
            Skills.Add(3, sinbrandActive);
            #endregion

            #region fourth active
            fistofOurosDebuff = new BuffDebuff()
            {
                Name = "fist_of_ouros_debuff",
                Obsolete_Duration = (self) => {
                    return fistofOurosKnockbackDuration;
                },
                //set a trigger to activate upon the unit colliding with terrain. Once it does so, stun for a set duration
            };

            var fistOfOurosActive = new Castable()
            {
                Name = "the_fist_of_ouros",
                IsUltimate = true,
                Cooldowns = fistofOurosCooldown,
                OnNonTargetedCast = (Castable ability, Unit caster) => {
                    //need to change this to be a line, rather than a radial thing, but it works for now, I guess...
                    float totalDamage = fistofOurosBaseDamage[ability.Level - 1] + (fistofOurosPowerScaling * caster.Power);
                    ForEnemiesInRadius(fistofOurosWidth, caster, (Unit enemy) => {
                        enemy.Knockback(caster.Position, fistofOurosKnockbackDuration, fistofOurosKnockbackDistance, fistofOurosKnockbackHeight);
                        caster.DealDamage(enemy, new DamagePacket { MagicalDamage = totalDamage });
                        enemy.AddBuff(fistofOurosDebuff);

                        caster.PlayAnimation("Ability4");
                    });
                }
            };
            Skills.Add(4, fistOfOurosActive);
            #endregion
        }
    }
}