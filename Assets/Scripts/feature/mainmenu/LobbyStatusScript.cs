﻿using core.http;
using feature.mainmenu.models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyStatusScript : MonoBehaviour {
    public Uri LobbyJoinEndpoint { get { return new Uri(NetworkStore.BaseUrl + "/api/Queue/Join"); } }
    public Uri LobbyStatusEndpoint { get { return new Uri(NetworkStore.BaseUrl + "/api/Queue/Status"); } }
    public Uri LobbyAcceptEndpoint { get { return new Uri(NetworkStore.BaseUrl + "/api/Queue/AcceptLobby"); } }
    private bool checkingQueueStatus;
    private bool shouldCheckQueueStatus;

    public GameObject LobbyStatusText;
    public GameObject AcceptLobbyButton;

    public GameObject ErrorPanel;
    private MainMenuErrorScript ErrorLogging {
        get {
            return ErrorPanel.GetComponent<MainMenuErrorScript>();
        }
    }

    public void Start() {
    }

    public void JoinLobby() {
        var httppost = new HttpPost() {
            Endpoint = LobbyJoinEndpoint,
            JsonModel = new SessionModel() {
                StringIdentifier = NetworkStore.SessionIdentifer
            }
        };

        StartCoroutine(httppost.CoSend((response) => {
            print(response);
            if (response.Status == HttpStatusCode.OK) {
                shouldCheckQueueStatus = true;
            } else {
                ErrorLogging.Throw(response.Data);
            }
        }));
    }

    public void AcceptLobby() {
        var httppost = new HttpPost() {
            Endpoint = LobbyAcceptEndpoint,
            JsonModel = new SessionModel() {
                StringIdentifier = NetworkStore.SessionIdentifer
            }
        };

        StartCoroutine(httppost.CoSend((response) => {
            if (response.Status == HttpStatusCode.OK) {
                print(response.Status + "," + response.Data); //TODO: Something else
            } else {
                ErrorLogging.Throw(response.Data);
            }
        }));
    }

    string LatestLobbyStatus = "";

    bool loadMainGame = false;

    void Update() {

        if (shouldCheckQueueStatus && !checkingQueueStatus) {
            checkingQueueStatus = true;
            var httppost = new HttpPost() {
                Endpoint = LobbyStatusEndpoint,
                JsonModel = new SessionModel() {
                    StringIdentifier = NetworkStore.SessionIdentifer
                }
            };

            StartCoroutine(httppost.CoSend((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    if(LatestLobbyStatus.Contains("\"PId\"")) {
                        if(File.Exists(".matchmaking")) {
                            File.Delete(".matchmaking");
                        }
                        AetherForgedNetworkManager.clientGameServerReference = response.FromJson<GameServerModel>();
                        loadMainGame = true;
                    } else {
                        LatestLobbyStatus = response.Data;
                        checkingQueueStatus = false;
                    }
                }
            }));
        }

        if(loadMainGame) {
            AetherForgedNetworkManager.clientTargetPort = AetherForgedNetworkManager.clientGameServerReference.Port;
            AetherForgedNetworkManager.clientTargetIp = AetherForgedNetworkManager.clientGameServerReference.IpAddress;

            SceneManager.LoadScene("AetherForged");
        }

        LobbyStatusText.GetComponent<Text>().text = "Lobby Status\n" + LatestLobbyStatus;

        if (LatestLobbyStatus.StartsWith("Waiting for players to accept:")) {
            AcceptLobbyButton.SetActive(true);
        } else {
            AcceptLobbyButton.SetActive(false);
        }
    }
}
