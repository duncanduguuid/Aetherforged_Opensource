﻿using System;
using System.Collections.Generic;

namespace core
{
    [Serializable]
    public class DisablingFeatures
    {
        public string NameOfStatus;
        public bool Movement;
        public bool Casting;
        public bool Cripple;

        public virtual DisablingFeatures Add(DisablingFeatures b)
        {
            var output = new DisablingFeatures
            {
                Movement = Movement || b.Movement,
                Casting = Casting || b.Casting,
                Cripple = Cripple || b.Cripple
            };
            return output;
        }

        public static DisablingFeatures operator + (DisablingFeatures a, DisablingFeatures b) { return a.Add(b); }

        public virtual bool Equals(DisablingFeatures b)
        {
            return Movement == b.Movement && Casting == b.Casting && Cripple == b.Cripple;
        }

        public static bool operator == (DisablingFeatures a, DisablingFeatures b) { return a.Equals(b); }
        public static bool operator != (DisablingFeatures a, DisablingFeatures b) { return !a.Equals(b); }
    }

    [Serializable]
    public class DisablesList
    {
        protected Unit unit;
        public DisablingFeatures Disablings { get; set; }
        public List<string> MovementDisablingStatuses { get; set; }
        public List<string> CastingDisablingStatuses { get; set; }
        public List<string> CrippleDisablingStatuses { get; set; }

        public DisablesList(Unit myUnit)
        {
            this.Disablings = new DisablingFeatures();
            this.MovementDisablingStatuses = new List<string>();
            this.CastingDisablingStatuses = new List<string>();
            this.CrippleDisablingStatuses = new List<string>();
        }

        public void Add(DisablingFeatures b)
        {
            this.Disablings += b;

            if (b.Movement && !MovementDisablingStatuses.Contains(b.NameOfStatus)) MovementDisablingStatuses.Add(b.NameOfStatus);
            if (b.Casting && !CastingDisablingStatuses.Contains(b.NameOfStatus)) CastingDisablingStatuses.Add(b.NameOfStatus);
            if (b.Cripple && !CrippleDisablingStatuses.Contains(b.NameOfStatus)) CrippleDisablingStatuses.Add(b.NameOfStatus);
        }

        public void Remove(DisablingFeatures b)
        {
            if(b.Movement && MovementDisablingStatuses.Contains(b.NameOfStatus))
            {
                if (MovementDisablingStatuses.Count == 1)
                {
                    Disablings.Movement = false;
                }
                MovementDisablingStatuses.Remove(b.NameOfStatus);
            }

            if (b.Casting && CastingDisablingStatuses.Contains(b.NameOfStatus))
            {
                if (CastingDisablingStatuses.Count == 1)
                {
                    Disablings.Casting = false;
                }
                CastingDisablingStatuses.Remove(b.NameOfStatus);
            }

            if (b.Cripple && CrippleDisablingStatuses.Contains(b.NameOfStatus))
            {
                if (CrippleDisablingStatuses.Count == 1)
                {
                    Disablings.Cripple = false;
                }
                CrippleDisablingStatuses.Remove(b.NameOfStatus);
            }
        }
    }
}