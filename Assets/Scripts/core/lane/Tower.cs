﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using core;
using core.forger;
using controllers;
using managers;
using feature;

namespace core.lane {
	public class Tower : Unit {
		public override UnitKind Kind {
			get { return UnitKind.Tower; }
		}

        public override BountyType Bounty {
            get { return BountyType.Global; }
        }

        public GameObject attackIndicator;

		public int Tier {
			get { return Level; }
		}

        public void SetTier (int tier ) {
            level = tier;
        }

        protected override void AnnounceDeath(Unit Killer)
        {
            Unit killer = Killer.GetComponent<Unit>();
            Tower myself = GetComponent<Tower>();
            AnnouncerEvent currEvent = new AnnouncerEvent(AnnouncementType.TowerDestroyed, "obelisk has been destroyed", killer, myself);
            AnnouncerController.Instance.QueueEvent(currEvent);
        }

        protected override void Initialize()
        {

            attackIndicator = UnityEngine.Object.Instantiate(Resources.Load("Towers/" + "lazor ('grammer art)") as GameObject,transform);

            base.Initialize();
        }

        protected override void Deactivate()
        {
            base.Deactivate();
            Destroy(attackIndicator);
        }

        public override void Colourize () {
            var bars = transform.Find("TowerCanvas").GetComponentsInChildren<Image>();

            if (modelReady)
            {
                //var rend = transform.Find("TowerCap(Clone)/crystal1").GetComponent<Renderer>();

                var colour = managers.UnitManager.Instance.AppropriateColour(Team);


                foreach (var bar in bars)
                {
                    if (bar.name == "HealthBar")
                    {
                        bar.color = colour;
                    }
                }

                // rend.material.SetColor("_EmissionColor", colour);

                // rend.material.color = colour;
            }
        }

        [ClientRpc]
        public void RpcAttackIndicator(Vector3 targetPosition)
        {

            var startPosition = attachPoint.position;
            Vector3 centerPos = (targetPosition + startPosition) / 2f;
            var distance = Vector3.Distance(targetPosition, startPosition);
            attackIndicator.transform.position = centerPos;
            attackIndicator.transform.rotation = Quaternion.LookRotation(targetPosition - startPosition);
            attackIndicator.transform.localScale = new Vector3(0.1f, 0.1f, distance);

        }


        [ClientRpc]
        public void RpcShowAttackIndicator()
        {
            attackIndicator.SetActive(true);
        }


        [ClientRpc]
        public void RpcHideAttackIndicator()
        {
            attackIndicator.SetActive(false);
        }

	}

}