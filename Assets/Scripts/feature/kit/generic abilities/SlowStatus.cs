﻿using UnityEngine;
using core;
using System.Collections.Generic;

namespace feature
{
    [CreateAssetMenu(fileName = "New Slow", menuName = "Passives/Generic/Slow")]
    public class SlowStatus : Status
    {
        [SerializeField] float fullDuration = 1f;

        [SerializeField] float initialSlowPercent = 10f;
        [SerializeField] float finalSlowPercent = 10f;
        [SerializeField] bool isFlat = false;
        [SerializeField] bool decays = false;
        [SerializeField] AnimationCurve curve;

        public bool IsFlat
        {
            get
            {
                return isFlat;
            }
        }

        public SlowStatus()
        {
            
            Name = "Slow";
            DurationOnApply = (holder) =>
            {
                return fullDuration;
            };
            CanBeCleansed = true;
            AffectedByTenacity = true;
            Disables = new DisablingFeatures
            {
                NameOfStatus = "Slow",
                Movement = false,
                Casting = false,
                Cripple = false
            };
            RelevantNumbers = new Dictionary<string, float>();
            RelevantNumbers["Initial Slow Number"] = initialSlowPercent;
            RelevantNumbers["Is Flat Slow"] = (isFlat) ? 1f : 0f;

            OnApply = (SourcePair statusDebuff, Unit targetUnit) =>
            {
                var st = (Status)statusDebuff.ability;
                targetUnit.ApplyDisablingFeatures((st).Disables);
                targetUnit.PushSlow(statusDebuff);
            };
            OnRemove = (SourcePair statusDebuff, Unit targetUnit) =>
            {
                var st = (Status)statusDebuff.ability;
                targetUnit.RemoveDisablingFeatures((st).Disables);
                targetUnit.RemoveSlow(statusDebuff);
            };
            OnTick = (SourcePair pair, Unit holder, PassiveTracker arg3, float arg4) => {
                holder.PushSlow(pair);
                if (decays)
                {
                    holder.PullSlow(pair);
                }
            };
        }

        public float SlowAmountAtTime(float timeSlice)
        {
            return initialSlowPercent + (finalSlowPercent - initialSlowPercent) * curve.Evaluate(timeSlice/fullDuration);
        }
    }
}