﻿using UnityEngine;
using Action = System.Action;

namespace core.utility
{
  public class Ticker
  {
    float lastCheck;
    float accumilatedTime;
    float tickDuration;

    public float TicksPerSecond
    {
      get { return 1.0f / tickDuration; }
    }

    public Ticker( float tps )
    {
      lastCheck = GameTime.time;
      accumilatedTime = 0;
      tickDuration = 1.0f / tps;
    }

    public void Reset()
    {
      lastCheck = GameTime.time;
      accumilatedTime = 0;
    }

    public int Ticks()
    {
      var newCheck = GameTime.time;

      accumilatedTime += newCheck - lastCheck;

      lastCheck = newCheck;

      if( accumilatedTime < tickDuration )
        return 0;

      var amount = (int)( accumilatedTime / tickDuration );
      accumilatedTime -= ((float) amount) * tickDuration;

      return amount;
    }

    public void OnTick( Action fn )
    {
      for( int i = 0; i < Ticks(); i++ )
        fn();
    }
  }
}