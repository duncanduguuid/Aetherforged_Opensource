﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using core.utility;
using controllers;
using System.Collections.Generic;
using managers;

namespace feature.kit
{
    public class PlagueKit : Imprint
    {
        /*
        public override Obsolete_Archetype obsolete_archetype
        {
            get
            {
                return new Mage();
            }
        }
        */
        #region disturbing methods variables
        //disturbing methods base variables
        public float disturbingMethodsCooldown = 12;
        public int disturbingMethodsRadius = 150;
        //disturbing methods CC variables
        public float disturbingMethodsFearDuration = .5f;
        //disturbing methods buff variables
        public BuffDebuff disturbingMethodsImmunityBuff;
        #endregion

        #region malaise variables
        //malaise base variables
        public float[] malaiseCooldown = new float[] { 10, 9, 8, 7, 6 };
        public float malaiseRange = 725;
        //malaise projectile variables
        public float malaiseMissileSpeed = 1000;
        public string malaiseProjectile = "malaise_projectile";
        public bool malaiseMissileHitEnemy = false;
        //malaise recast variables
        public Timer malaiseReactivationTimer = null;
        public float malaiseGlobalCooldownDuration = .25f;
        public float malaiseReactivationTimerDuration = 2.5f;
        public bool malaiseIsRecast = false;
        public float malaiseCurrentRecasts = 0;
        public float malaiseMaxRecasts = 5;
        //malaise damage variables
        public float[] malaiseBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float malaiseDamagePowerScaling = .5f;
        //malaise CC variables
        public float malaiseSlowDuration = 1.5f;
        public float malaiseSlowDegradeStart = .75f;
        public float malaiseSlowDegradeDuration = .75f;
        public float[] malaiseSlowAmount = new float[] { .25f, .3f, .35f, .4f, .45f };
        //malaise debuff variables
        public static BuffDebuff malaiseDebuff;
        public float malaiseDebuffDuration = 10f;
        public float malaiseDebuffMaxStacks = 5;
        //malaise debuff CC variables
        public float malaiseStunDuration = .75f;
        public float malaiseStacksForStun = 3;
        #endregion

        #region bloodletting variables
        //bloodletting base variables
        public float[] bloodlettingCooldown = new float[] { 1, 1, 1, 1, 1 };
        //bloodletting buff variables
        public BuffDebuff bloodlettingBuff;
        public float[] bloodlettingBuffDuration = new float[] { 1, 1, 1, 1, 1 };
        public bool bloodlettingRemovedDebuff = false;
        //bloodletting buff boost variables
        public float bloodlettingBoostDuration = 1.5f;
        public float bloodlettingBoostDecayDuration = 1.5f;
        public float bloodlettingBoostAmount = .8f;
        #endregion

        #region moonbloom variables
        //moonbloom base variables
        public float[] moonbloomCooldown = new float[] { 1, 1, 1, 1, 1 };
        public float moonbloomRange = 450;
        public int moonbloomRadius = 125;
        //moonbloom timer variables
        public Timer moonbloomDelayTimer;
        public float moonbloomDelayDuration = .4f;
        //moonbloom damage variables
        public float[] moonbloomBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float moonbloomPowerScaling = .5f;
        //moonbloom mesmerize variables
        public float moonbloomMesDuration = 1.5f;
        #endregion

        #region patient zero variables
        //patient zero base variables
        public float[] patientZeroCooldown = new float[] { 1, 1, 1 };
        public float patientZeroRange = 375;
        public int patientZeroRadius = 300;
        //patient zero timer variables
        public Timer patientZeroDelayTimer;
        public float patientZeroSpreadDelay = .75f;
        //patient zero damage variables
        public float[] patientZeroBaseDamage = new float[] { 1, 1, 1 };
        public float patientZeroPowerScaling = .5f;
        //patient zero CC variables
        public float patientZeroCrippleDuration = 1.75f;
        #endregion
        /*
        public override string ObsoleteName { get { return "tespis"; } }
        */
        protected override void Initialize(Unit me)
        {
            #region passive ability
            disturbingMethodsImmunityBuff = new BuffDebuff()
            {
                Name = "disturbing_methods_immunity",
                Obsolete_Duration = (Passive self) => {
                    return disturbingMethodsCooldown;
                }
            };

            var disturbingMethodsPassive = new Passive()
            {
                Name = "disturbing_methods",
                //add for on unit kill as well, or maybe just tie them into one?
                Obsolete_OnForgerKillOrAssist = (Passive ability, Unit caster, Unit killedEnemy) => {
                    ForEnemiesInRadius(disturbingMethodsRadius, caster, (Unit target) => {
                        if (!target.HasBuff(disturbingMethodsImmunityBuff)) {
                            //implement once fears are implemented
                            //target.AddBuff(new Fear(), disturbingMethodsFearDuration);
                                target.AddBuff(disturbingMethodsImmunityBuff);
                            }
                        });
                    },
                    Obsolete_OnKillUnit = (Passive ability, Unit caster, Unit killedEnemy) => {
                        ForEnemiesInRadius(disturbingMethodsRadius, caster, (Unit target) => {
                            if (!target.HasBuff(disturbingMethodsImmunityBuff) && !(target is Forger))
                            {
                                //implement once fears are implemented
                                //target.AddBuff(new Fear(), disturbingMethodsFearDuration);
                                target.AddBuff(disturbingMethodsImmunityBuff);
                            }
                    });
                }
            };
            Passives.Add(0, disturbingMethodsPassive);
            #endregion

            #region first active
            malaiseDebuff = new BuffDebuff()
            {
                Name = "malaise_debuff",
                /*
                    Obsolete_OnApply = (Passive self, Unit source, Unit target) => {
                    /*reapply slow
                    target.AddBuff(new Slow(malaiseSlowAmount,malaiseSlowDuration,0,malaiseSlowDegradeDuration,malaiseSlowDegradeStart),malaiseSlowDuration);
                    */
                    /*
                    if (self.CurrentStacks == malaiseStacksForStun) {
                        /*Add a stun
                        target.AddBuff(new Stun(), malaiseStunDuration);

                    }
                        
                },
                Obsolete_Duration = (Passive self) => {
                    return malaiseDebuffDuration;
                }
                        */
            };

            var malaiseActive = new Castable()
            {
                Name = "malaise",
                Cooldowns = malaiseCooldown,
                GetIconSubtext = (Ability ability, Unit caster) => {
                    return 10.ToString();
                },
                OnNonTargetedCast = (ability, caster) => {

                    float baseDamage = malaiseBaseDamage[ability.Level - 1];
                    float totalDamage = baseDamage + (malaiseDamagePowerScaling * caster.Power);
                    malaiseMissileHitEnemy = false;
                    Vector2 locationTarget;
                    if (Utils.TryGetPlanarPointWithinRangeFromMousePosition(out locationTarget, caster.Position, malaiseRange, malaiseRange)) {
                        caster.PlayAnimation("Ability1");
                        ProjectileManager.Instance.Gimme(malaiseProjectile, locationTarget, caster, malaiseMissileSpeed
                            ,OnHitUnit: (proj, hit_unit) => {
                                if (hit_unit.Team != caster.Team) {
                                    caster.DealDamage(hit_unit, new DamagePacket { MagicalDamage = totalDamage });
                                    hit_unit.AddBuff(malaiseDebuff);
                                }
                            }
                        );
                        //set up missile logic so that if it hits an enemy, set the ability on the recast, otherwise start the full cooldown
                    }
                },
            };
            Skills.Add(1, malaiseActive);
            #endregion

            #region second active
            bloodlettingBuff = new BuffDebuff()
            {
                Name = "bloodletting_buff",
                //don't take slow/roots for duration of buff
                Obsolete_Duration = (Passive self) =>
                {
                    return bloodlettingBuffDuration[self.Parent.Level-1];
                }
            };

            var bloodlettingActive = new Castable()
            {
                Name = "bloodletting",
                Cooldowns = bloodlettingCooldown,
                OnNonTargetedCast = (ability, caster) => {
                    bloodlettingRemovedDebuff = false;
                    caster.RemoveDisablingFeatures(new DisablingFeatures {NameOfStatus = "Slow", Movement = true} );
                    caster.RemoveDisablingFeatures(new DisablingFeatures {NameOfStatus = "Root", Movement = true} );
//                    foreach (Ability buff in caster.Buffs) 
                        {
                        //implement this once we have the ability to check if a buff is a root or slow
                        /*if(buff is Root || buff is Slow) {
                            caster.RemoveBuff(buff);
                            bloodlettingRemovedBuff = true;
                        }*/
                    }

                    if (bloodlettingRemovedDebuff)
                    {
                        bloodlettingBuff.Parent = ability;
                        caster.AddBuff(bloodlettingBuff);
                    }
                    //to implement adding movement speed boosts
                    //caster.AddBuff(new Boost(bloodlettingBoostDuration, bloodlettingBoostAmount, bloodlettingBoostDecayDuration));
                }
            };
            Skills.Add(2, bloodlettingActive);
            #endregion

            #region third active
            var moonbloomActive = new Castable()
            {
                Name = "moonbloom",
                Cooldowns = moonbloomCooldown,
                OnNonTargetedCast = (ability, caster) =>
                {
                    Vector2 locationTarget;
                    moonbloomDelayTimer = new Timer();
                    if (Utils.TryGetPlanarPointWithinRangeFromMousePosition(out locationTarget, caster.Position, moonbloomRange))
                    {
                        float baseDamage = moonbloomBaseDamage[ability.Level - 1];
                        float totalDamage = baseDamage + (moonbloomPowerScaling * caster.Power);
                        moonbloomDelayTimer.Start(() =>
                        {
                            Unit.ForEachEnemyInRadius(moonbloomRadius, locationTarget, caster.Team, (Unit target) =>
                            {
                                caster.DealDamage(target, new DamagePacket { MagicalDamage = totalDamage });
                                //implement mesmerizes
                                //target.AddBuff(new Mesmerize(), moonbloomMesDuration);
                            });
                            return 0f;
                        }, moonbloomDelayDuration);
                    }
                }
            };
            Skills.Add(3, moonbloomActive);
            #endregion

            #region fourth active
            var patientZeroActive = new Castable()
            {
                Name = "patient_zero",
                Cooldowns = patientZeroCooldown,
                TargetedCastingConditions = Ability.UnitTargetShortcuts.EnemySimple,
                UnitTargetedCast = (Castable ability, Unit caster, Unit target) => {
                    float totalDamage = patientZeroBaseDamage[ability.Level - 1] + (patientZeroPowerScaling * caster.Power);
                    //set this up with a DoT
                    caster.DealDamage(target, new core.DamagePacket { MagicalDamage = totalDamage });
                    //implement cripples
                    //target.AddBuff(new Cripple(), patientZeroCrippleDuration);
                    patientZeroDelayTimer = new Timer();
                    patientZeroDelayTimer.Start(() => {
                        Unit.ForEachEnemyInRadius(patientZeroRadius, target.Position, caster.Team, (Unit enemy) => {
                            if (enemy != target)
                            {
                                /* see above notes for single character
                                enemy.DealDamage(enemy, new core.DamagePacket { MagicalDamage = totalDamage });
                                enemy.AddBuff(new Cripple(), patientZeroCrippleDuration);*/
                            }
                        });
                        return 0f;
                    }, patientZeroSpreadDelay);
                }
            };
            Skills.Add(4, patientZeroActive);
            #endregion
        }
    }
}