﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Tower Passive", menuName = "Ability/Lane/Tower Passive")]
    public class TowerPassive : Passive
    {
        public TowerPassive()
        {
            Name = "tower_";
            HolderIsStructure = true;
            OnBasicAttack = (SourcePair pair, Unit owner, Unit attackTarget, DamagePacket packet) =>
            {

                //  Melee Minion Damage: 40% maximum health
                //  Ranged Minion Damage: 90% maximum health

                if (attackTarget.Kind == UnitKind.Minion)
                {
                    switch (attackTarget.Spec)
                    {
                        case 'M':
                            packet.PhysicalDamage = 0;
                            packet.PureDamage = attackTarget.MaxHealth * 0.4f;
                            break;
                        case 'R':
                            packet.PhysicalDamage = 0;
                            packet.PureDamage = attackTarget.MaxHealth * 0.75f;
                            break;
                        default:
                            break;
                    }

                }
                else if (attackTarget.Kind == UnitKind.Forger)
                {
                    //                        owner.AddBuff(TowerShredBuff);

                }

                //                    {   // horrible animation thing.
                //                        var startPosition = attachPoint.position;
                //                        var targetPosition = target.transform.position;
                //                        Vector3 centerPos = (targetPosition + startPosition) / 2f;
                //                        var distance = Vector3.Distance(targetPosition, startPosition);
                //                        //                        var scaleX = Mathf.Abs(startPosition.x - targetPosition.x);
                //                        //                        var scaleY = Mathf.Abs(startPosition.y - targetPosition.y);
                //                        //                        var scaleZ = Mathf.Abs(startPosition.z - targetPosition.z);
                //
                //                        GameObject gob = UnityEngine.Object.Instantiate(Resources.Load("Towers/" + "lazor ('grammer art)") as GameObject);
                //                        gob.transform.position = centerPos;
                //                        gob.transform.rotation = Quaternion.LookRotation(targetPosition - startPosition);
                //                        gob.transform.localScale = new Vector3(0.1f, 0.1f, distance);
                //                        Destroy(gob, 0.2f);
                //                    }



                return packet;
            };
            DynamicStats = (pair, holder, data) => 
            {
                switch (holder.Level)
                {
                    case 1:
                        return new UnitStats();
                    case 2:
                        return UnitStats.Stats(
                            MaxHealth: 1500,
                            AutoAttackDamage: 100
                        );
                    default:
                        return UnitStats.Stats(
                            MaxHealth: 1000f * holder.Level,
                            AutoAttackDamage: 50f * holder.Level
                        );
                }
            };

        }
    }
}