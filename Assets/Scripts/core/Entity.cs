﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using core.utility;

namespace core {
    public class Entity : NetworkBehaviour {

        public virtual UnitKind Kind {
            get { return UnitKind.Unknown; }
        }

        public Vector2 Position {
            get {
                return Utils.PlanarPoint(gameObject.transform.position);
            }
            internal set {
                var pos = gameObject.transform.position;
                pos.x = value.x * Constants.TO_UNITY_UNITS;
                pos.z = value.y * Constants.TO_UNITY_UNITS;
                gameObject.transform.position = pos;
            }
        }

        [SerializeField]
        protected float radius; 
        public float Radius {
            get { return radius; }
        }

        [SerializeField]
        [SyncVar]
        Team team = Team.Neutral;
        public Team Team {
            get { return team; }
            set { team = value; }
        }

        public virtual DamagePacket DealDamage(Unit targetUnit, DamagePacket packet) {
            targetUnit.TakeDamage(packet);
            return packet;
        }


        public virtual void ProvideHeal(Unit targetUnit, HealPacket packet) {
            targetUnit.ReceiveHeal(packet);
        }

        protected virtual DamagePacket TakeDamage(DamagePacket packet) {
            return packet;
        }


        public bool TeamQualifiesAs(TeamQualifier qual, Team relTeam) {
            bool enemies = false;
            bool neutral = false;
            bool friendly = false;

            if (team == Team.Neutral)
                neutral = true;

            if ((team == Team.UpRight && relTeam == Team.DownLeft)
                || (team == Team.DownLeft && relTeam == Team.UpRight)
                || (neutral && relTeam != Team.Neutral))
                enemies = true;
            else if (!neutral && relTeam != Team.Neutral)
                friendly = true;

            if ((qual & TeamQualifier.EnemyTeam) != 0 && enemies)
                return true;
            if ((qual & TeamQualifier.FriendlyTeam) != 0 && friendly)
                return true;
            if ((qual & TeamQualifier.NeutralTeam) != 0 && neutral)
                return true;

            return false;
        }


        public static IEnumerable<Unit> InRadius (float radius, Vector2 position) {
            var units = Unit.ListOf.Where(unit => {
                return Utils.CollidesWith(unit.Position, unit.Radius, position, radius);
            });

            return units;
        }

        public static IEnumerable<Unit> ForEachInRadius (float radius, Vector2 position, Action<Unit> action = null) {
            var units = InRadius(radius, position);

            if (action != null) {
                foreach (Unit unit in units) {
                    action(unit);
                }
            }
            return units;
        }

        public static IEnumerable<Unit> ForEachAllyInRadius( float radius, Vector2 position, Team team, Action<Unit> action = null) {
            var units = Unit.ListOf.Where((unit) => {
                return unit.Team == team && Utils.CollidesWith(unit.Position, unit.Radius, position, radius);
            });

            if (action != null) {
                foreach (Unit unit in units) {
                    action(unit);
                }
            }
            return units;
        }

        //TODO: Make it possible to easily target where the mouse cursor is located
        public static IEnumerable<Unit> ForEachEnemyInRadius( float radius, Vector2 position, Team team, Action<Unit> action = null) {
            var units = Unit.InRadius(radius, position).Where((unit) => {
                return unit.Team != team && Utils.CollidesWith(unit.Position, unit.Radius, position, radius);
            });

            if (action != null) {
                foreach (Unit unit in units) {
                    action(unit);
                }
            }
            return units;
        }

        public static IEnumerable<Unit> ForEachEnemyInLine( float length, float width, Vector2 startPos, Vector2 endPos, Team team, Action<Unit> action = null) {
            var units = Utils.GetUnitsInLine(startPos, endPos, width, length).Where((unit) => {
                return unit.Team != team;
            });

            if (action != null) {
                foreach(Unit unit in units) {
                    action(unit);
                }
            }
            return units;
        }

        public static IEnumerable<Unit> ForEachAllyInLine(float length, float width, Vector2 startPos, Vector2 endPos, Team team, Action<Unit> action = null)
        {
            var units = Utils.GetUnitsInLine(startPos, endPos, width, length).Where((unit) => {
                return unit.Team == team;
            });

            if (action != null)
            {
                foreach (Unit unit in units)
                {
                    action(unit);
                }
            }
            return units;
        }

        public static IEnumerable<Unit> ForEachUnitInLine(float length, float width, Vector2 startPos, Vector2 endPos, Team team, Action<Unit> action = null)
        {
            var units = Utils.GetUnitsInLine(startPos, endPos, width, length);

            if (action != null)
            {
                foreach (Unit unit in units)
                {
                    action(unit);
                }
            }
            return units;
        }

        public static IEnumerable<Unit> ForEachEnemyInCone(float arc, float length, Vector2 startPos, Vector2 endPos, Team team, Action<Unit> action = null)
        {
            var units = Utils.GetUnitsInCone(startPos, endPos, arc, length).Where((unit) => {
                return unit.Team != team;
            });

            if (action != null)
            {
                foreach (Unit unit in units)
                {
                    action(unit);
                }
            }
            return units;
        }

        public static IEnumerable<Unit> ForEachAllyInCone(float arc, float length, Vector2 startPos, Vector2 endPos, Team team, Action<Unit> action = null)
        {
            var units = Utils.GetUnitsInCone(startPos, endPos, arc, length).Where((unit) => {
                return unit.Team == team;
            });

            if (action != null)
            {
                foreach (Unit unit in units)
                {
                    action(unit);
                }
            }
            return units;
        }

        public static IEnumerable<Unit> ForEachUnitInCone(float arc, float length, Vector2 startPos, Vector2 endPos, Team team, Action<Unit> action = null)
        {
            var units = Utils.GetUnitsInCone(startPos, endPos, arc, length);

            if (action != null)
            {
                foreach (Unit unit in units)
                {
                    action(unit);
                }
            }
            return units;
        }
    }

    #region prereqs
    public enum Lane {
        Top,
        Bottom
    }

    public enum UnitKind {
        Unknown,
        Forger,
        Minion,
        Tower,
        Monster,
        LargeMonster,
        Miner
    }

    public enum BountyType {
        /// <summary>
        /// All allies get some. 
        /// </summary>
        Global,
        /// <summary>
        /// Use the Split Multiplier formula to evenly distribute amongst all nearby allies.
        /// </summary>
        Local,
        /// <summary>
        /// Killer gets full reward. Assists split half that evenly.
        /// </summary>
        Assist,
        /// <summary>
        /// Killer gets full reward. nearby allies get an even split of a percentage of that. 
        /// </summary>
        Runnoff,
        /// <summary>
        /// Killer gets full reward. No exceptions. 
        /// </summary>
        Solo
    }
    #endregion
}
