﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace core.utility
{
  public class EventEmitter<T, E>
  {
    public delegate void EventHandler( T target, E evnt );

    event EventHandler Event;

    public EventEmitter()
    {
    }

    public void Emit( T target, E evnt )
    {
      if( Event != null )
        Event( target, evnt );
    }
  
    public void AddHandler( EventHandler handler ) 
    {
      // TODO 4/11/15: Any actual network synchronization event
      Event += handler;
    }

    public void RemoveHandler( EventHandler handler )
    {
      // TODO 4/11/15: Any actual network synchronization event
      Event -= handler;
    }

    public bool RemoveHandler( UInt32 listenerId )
    {
      // TODO 4/11/15: Any actual network synchronization event
      bool wasRemoved = false;

      EventHandler[] listeners = Event.GetInvocationList() as EventHandler[];

            /* Todo: what
      foreach( EventHandler listener in toBeRemoved )
      {
        wasRemoved = true;
        Event -= listener;
      }
      */

      return wasRemoved;
    }

  }
}