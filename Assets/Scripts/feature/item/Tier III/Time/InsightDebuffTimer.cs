﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Insight Debuff Timer", menuName = "Passives/Items/Insight Debuff Timer")]
    public class InsightDebuffTimer : BuffDebuff
    {
        [SerializeField]
        float InsightImmunityDuration = 10;
        [SerializeField]
        int InsightDebuffStackPerRanged = 1;
        [SerializeField]
        int InsightDebuffStackPerMelee = 2;
        [SerializeField]
        float InsightDebuffDuration = 4;
        [SerializeField]
        int InsightDebuffMaxStacks = 5;
        [SerializeField]
        float InsightRootDuration = 1;

        [SerializeField]
        BuffDebuff InsightDebuff;

        public InsightDebuffTimer()
        {
            Name = "Insight debuff timer";
            AlsoObsolete_Duration = (pair) =>
            {
                return InsightDebuffDuration;
            };
            OnRemove = (SourcePair pair, Unit owner) =>
            {
                owner.RemoveBuff(InsightDebuff.WithSource(null), owner.StacksOf(InsightDebuff.WithSource(null)));
            };
            BeforeTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, DamagePacket packet) =>
            {
                if (packet.Kind == DamageActionType.AutoAttack && packet.Source.Kind == UnitKind.Forger)
                {
                    if (packet.Source.AttackRange < Constants.MIDRANGE_DEFAULT_ATTACK_RANGE)
                    {
                        owner.AddBuff(InsightDebuff.WithSource(null), InsightDebuffStackPerMelee);
                    }
                    else
                    {
                        owner.AddBuff(InsightDebuff.WithSource(null), InsightDebuffStackPerRanged);
                    }
                    if (owner.StacksOf(InsightDebuff.WithSource(null)) >= InsightDebuff.MaximumStacks)
                    {
                        owner.RemoveBuff(InsightDebuff.WithSource(null), fullClear: true);
                        //TODO: Add root status to the unit. Presumably needs a unique effect like ApplySlow
                    }
                }
                return packet;
            };
        }
    }
}