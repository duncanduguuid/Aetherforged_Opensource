﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using managers;

using core;

namespace ui {

    public class UIManager : Singleton<UIManager> {

        public GameObject OptionsMenuPanel;
        public GameObject ShopMenuPanel;

        static bool isShopMenuOpen;
        static bool isOptionsMenuOpen;
        public bool IsOptionsMenuOpen {
            get { return OptionsMenuPanel.activeInHierarchy; }
            set {
                isOptionsMenuOpen = value;
                OptionsMenuPanel.SetActive(isOptionsMenuOpen);
            }
        }
        public bool IsShopMenuOpen {
            get {return isShopMenuOpen; }
            set {
                isShopMenuOpen = value;
                ShopMenuPanel.SetActive(isShopMenuOpen);
            }
        }

        public bool IsMenuOpen {
            get { return IsOptionsMenuOpen || IsShopMenuOpen; }
        }

        void Start() {
            instance = this;
            OptionsMenuPanel.SetActive(true);
            OptionsMenuPanel.SetActive(false);
            ShopMenuPanel.SetActive(true);
            ShopMenuPanel.SetActive(false);
        }

        void Update() {
            if (KeyConfig.Instance.WentDown(ICE.BackOptionMenu)) {
                ToggleOptionsMenu();
            }
            if (KeyConfig.Instance.WentDown(ICE.ToggleWindowShop)) {
                ToggleShopMenu();
            }
        }

        public void ToggleOptionsMenu() {
            if (IsShopMenuOpen)
            {
                IsShopMenuOpen = false;
            }
            IsOptionsMenuOpen = !IsOptionsMenuOpen;
        }

        public void ToggleShopMenu() {
            IsShopMenuOpen = !isShopMenuOpen;
        }

    }

}