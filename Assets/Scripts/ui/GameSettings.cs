﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings {

    public static int fullscreen;
    public static int FPS;
    public static int textureQuality;
    public static int antiAiliasing;
    public static int vSync;
    public static int resolutionIndex;
    public static float masterVolume;
    public static float musicVolume;
    public static float effectsVolume;
    public static float ambienceVolume;
    public static float voiceVolume;
    public static float announcerVolume;
}
