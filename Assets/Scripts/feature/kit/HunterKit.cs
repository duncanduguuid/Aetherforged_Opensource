﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using controllers;
using System.Collections.Generic;
using managers;
using core.utility;

namespace feature.kit
{
    public class HunterKit : Imprint
    {
        /*
        public override Obsolete_Archetype obsolete_archetype
        {
            get
            {
                return new RangedCarry();
            }
        }
        */
        //trophy hunter base variables
        
        //trophy hunter buff variables
        public static BuffDebuff TrophyHunterBuff;
        public float trophyHunterBuffDuration = 8.0f;
        public float trophyHunterRemainingAttacks = 0;
        public float trophyHunterBaseAttacks = 3;
        public float trophyHunterPerHealthRatio = 800;
        public float[] trophyHunterBaseDamage = new float[] { 15, 28, 41, 54, 67, 80 };
        public float trophyHunterDamagePerHealth = 100;
        public float trophyHunterDamagePercentIncrease = .015f;

        //cold steel base variables
        public float[] cleavingZephyrCooldown = new float[] { 10, 10, 10, 10, 10 };
        //cold steel shield variables
        public float[] coldSteelShieldValue = new float[] { 30, 45, 60, 75, 90 };
        public float[] coldSteelShieldMaxShield = new float[] { 90, 135, 180, 225, 270 };
        public float coldSteelShieldDuration = 2.0f;
        public static ShieldBuff ColdSteelShield;
        //cold steel buff variables
        public static BuffDebuff ColdSteelBuff;
        public float coldSteelBuffDuration = 5.0f;
        public float coldSteelCooldownReduction = .4f;
        public float coldSteelCurrentCooldownReduction = 0;
        //cold steel buff resource variables
        public float coldSteelResourceCost = 20;
        public float coldSteelBuffMaxHits = 2;
        public float coldSteelBuffRemainingHits = 2;

        //prototype flare base variables
        public float[] prototypeFlareCooldown = new float[] { 1, 1, 1, 1, 1 }; //needs numbers
        public float prototypeFlareRange = 3000;
        //prototype flare projectile variables
        public float prototypeFlareMissileSpeed = 1000;
        public float prototypeFlareMissileWidth = 50;
        public string prototypeFlareProjectile = "prototype_flare_projectile";
        //prototype flare vision variables
        public float prototypeFlareVisionRadius = 300;
        public float prototypeFlareVisionDuration = 15;

        //precision shot base variables
        public float[] precisionShotCooldown = new float[] { 1, 1, 1, 1, 1 }; //needs numbers
        public float precisionShotRange = 850;
        //precision shot damage variables
        public float[] precisionShotBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float precisionShotPowerScaling = .2f;
        //precision shot projectile variables
        public float precisionShotMissileSpeed = 1000;
        public float precisionShotMissileWidth = 225;
        public string precisionShotProjectile = "precision_shot_projectile";
        //precision shot debuff variables
        public static BuffDebuff precisionShotDebuff;
        public float precisionShotMainDebuffDuration;


        //alvkhret's honor basic variables
        public float[] alvkhretsHonorCooldown = new float[] { 1, 1, 1 }; //needs numbers
        public float alvkhretsHonorBaseRange = 625;
        //alvkhret's honor projectile variables
        public float alvkhretsHonorMissileSpeed = 900;
        public float alvkhretsHonorMissileWidth = 75;
        public string alvkhretsHonorProjectile = "alvkhrets_honor_projectile";
        public Unit avlkhretsHonorFirstHit;
        //alvkhret's honor damage variables
        public float[] alvkhretsHonorBaseDamage = new float[] { 1, 1, 1 };
        public float alvkhretsHonorPowerScaling = .2f;
        //alvkhret's honor blink variables
        public float alvkhretsHonorBlinkRange = 450;
        //alvkhret's honor blink timer variables
        public Timer alvkhretsHonorBlinkTimer;
        public float alvkhretsHonorBlinkDelay = .2f;
        /*
        public override string ObsoleteName { get { return "nibra"; } }
        */
        protected override void Initialize(Unit me)
        {
            var ParticleColdSteelIceflames = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberQ/EmberQ_iceflames",
                new string[] { "handAttachment .l", "handAttachment .r" }
            );
            var ParticleWildfireWhirl = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberW/EmberWwhirl",
                "chest"
            );
            var ParticleWildfireTrail = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberW/EmberW_trail",
                new string[] { "handAttachment .l", "handAttachment .r" }
            );
            var ParticleAvalanceTrail = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberE/EmberE_ability_trail",
                "NibraArmature"
            );
            var ParticleAvalanceMainHit = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberE/EmberE_maintarget_hit",
                "chest"
            );
            var ParticleAvalanceSlashThroughHit = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberE/EmberE_slashtrough_hit",
                "chest"
            );
            var ParticleUltGroundEffect = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberR/NibraR_particle",
                ""
            );
            var ParticleUltDebuff = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberR/NibraR_debuff",
                "chest"
            );

            #region passive ability

            TrophyHunterBuff = new BuffDebuff()
            {
                Name = "trophy_hunter_buff",
                OnBasicAttack = (pair, holder, target, packet) => 
                {
                    if (target.Kind == UnitKind.Forger || target.Kind == UnitKind.LargeMonster)
                    {
                        float totalPercent = (float)(Math.Floor(target.MaxHealth / trophyHunterDamagePerHealth)) * trophyHunterDamagePercentIncrease;
                        float finalDamage = totalPercent * trophyHunterBaseDamage[pair.ParentAbilityLevelLessOne];
                        trophyHunterRemainingAttacks--;
                        holder.DealDamage(target, new DamagePacket(DamageActionType.SkillOrAbility, holder) { PhysicalDamage = finalDamage });
                        if (trophyHunterRemainingAttacks <= 0)
                        {
                            holder.RemoveBuff(pair);
                        }
                    }
                    return packet;
                },
                /*
                Obsolete_OnRemove = (Passive self, Unit caster, Unit buffedUnit) => {
                    trophyHunterRemainingAttacks = 0;
                },
                */
                Obsolete_Duration = (Passive self) => {
                    return trophyHunterBuffDuration;
                }
            };

            var trophyHunterPassive = new Passive()
            {
                Name = "trophy_hunter",
                Obsolete_OnForgerKillOrAssist = (Passive ability, Unit caster, Unit target) => {
                    float tempAttacks = trophyHunterBaseAttacks + (float)(Math.Floor(target.MaxHealth / trophyHunterPerHealthRatio));
                    if (trophyHunterRemainingAttacks <= tempAttacks) {
                        trophyHunterRemainingAttacks = tempAttacks;
                    }
                    caster.AddBuff(TrophyHunterBuff);
                },
            };
            Passives.Add(0, trophyHunterPassive);
            #endregion

            #region first active

            var cleavingZephyrActive = new Castable()
            {
                Name = "cleaving_zephyr",
                Cooldowns = cleavingZephyrCooldown,
            };
            Skills.Add(1, cleavingZephyrActive);
            #endregion

            #region second active
            var prototypeFlareActive = new Castable()
            {
                Name = "prototype_flare",
                Cooldowns = prototypeFlareCooldown,
                OnNonTargetedCast = (Castable self, Unit caster) =>
                {
                    caster.PlayAnimation("Ability2");
                    //set up missile logic so that it allows a recast of the ability if a unit is targeted by the W

                    Vector2 locationTarget;
                    if (Utils.TryGetPlanarPointWithinRangeFromMousePosition(out locationTarget, caster.Position, prototypeFlareRange, prototypeFlareRange))
                    {

                        ProjectileManager.Instance.Gimme(prototypeFlareProjectile, locationTarget, caster, prototypeFlareMissileSpeed);
                    }
                }
            };
            Skills.Add(2, prototypeFlareActive);
            #endregion

            #region third active
            precisionShotDebuff = new BuffDebuff {
                Name = "precision_shot_debuff",
            };
            var precisionShotActive = new Castable()
            {
                Name = "precision_shot",
                Cooldowns = precisionShotCooldown,
                OnNonTargetedCast = (Castable self, Unit caster) =>
                {
                    float baseDamage = precisionShotBaseDamage[self.Level - 1];
                    float totalDamage = baseDamage + (precisionShotPowerScaling * caster.Power);
                    caster.PlayAnimation("Ability3");
                    //set up missile logic so that it allows a recast of the ability if a unit is targeted by the W

                    Vector2 locationTarget;
                    if (Utils.TryGetPlanarPointWithinRangeFromMousePosition(out locationTarget, caster.Position, precisionShotRange, precisionShotRange))
                    {

                        ProjectileManager.Instance.Gimme(prototypeFlareProjectile, locationTarget, caster, prototypeFlareMissileSpeed
                            , OnHitUnit: (proj, hit_unit) => {
                                caster.DealDamage(hit_unit, new DamagePacket { PhysicalDamage = totalDamage });
                                hit_unit.AddBuff(precisionShotDebuff);
                            });
                    }
                }
            };
            Skills.Add(3, precisionShotActive);
            #endregion

            #region fourth active
            var AlvkhretsHonorActive = new Castable()
            {
                Name = "Alvkhret's Honor",
                Cooldowns = alvkhretsHonorCooldown,
            };
            Skills.Add(4, AlvkhretsHonorActive);
            #endregion
        }
    }
}