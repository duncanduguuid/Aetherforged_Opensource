﻿using System;
using UnityEngine;
using UnityEngine.Networking;

using core;
using core.utility;

namespace controllers {
    public class AnimationController : MonoBehaviour {
        Animator anim;
        Unit owner;

        private float Velocity;
        private bool BasicAttack;

        private bool death;

        void Start() {
            if (owner == null) {
                owner = GetComponentInParent<Unit>();
            }

            if (owner == null) {
                owner = GetComponent<Unit>();
            }

            if (owner == null) {
                enabled = false;
            }

            if (anim == null) {
                anim = GetComponentInChildren<Animator>();
            }

            if (anim == null) {
                anim = GetComponent<Animator>();
            } 
        }

        void Update() {
            if (owner != null && anim != null) {
                anim.SetFloat("Velocity", owner.Velocity.magnitude / Time.deltaTime);
                anim.SetFloat("AttackSpeed", owner.AnimationAttackSpeed);

                death = anim.GetBool("Dead");
                if (owner.IsDead != death) {
                    anim.SetBool("Dead", owner.IsDead);
                }
            } else {
                // if the owner has been destroyed, we probably need to die. 
                anim.SetBool("Dead", true);
                // TODO 2017 July 9 Should we "clean up" here or maybe unify this clean up?
                Destroy(gameObject, 30f);
            }


        }

        public void SetAngle(float angle) {
			_set_angle(angle);
        }

        public void Play(string animName)
        {
            _play(animName);
        }

        private void _set_angle( float angle)
        {
            if (anim != null)
                anim.SetFloat("AbilityAngle", angle);
        }

        private void _play(string animName)
        {
            if (anim != null)
                anim.SetTrigger(animName);
        }
    }
}
