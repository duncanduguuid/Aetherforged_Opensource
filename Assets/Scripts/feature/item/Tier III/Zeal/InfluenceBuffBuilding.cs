﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Influence Buff Building", menuName = "Passives/Items/Influence Buff Building")]
    public class InfluenceBuffBuilding : BuffDebuff
    {
        public int InfluenceBuffBuildingMaxStacks = 3;
        public float InfluenceSpeedBuffIncrease = 30;
        public float InfluenceSpeedBuffDuration = 2;
        public float InfluencePassiveBonusAttackDamage = .5f;
        public float InfluenceMarkDuration = 6;


        public InfluenceBuffBuilding()
        {
            Name = "Influence building";
            MaximumStacks = InfluenceBuffBuildingMaxStacks;

        }
    }
}