﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
	[CreateAssetMenu(fileName = "Substance Passive", menuName = "Passives/Items/Substance")]
	public class SubstancePassive : Passive
	{
		[SerializeField]
		float SubstancePassiveBonusArmorPercentage;
		[SerializeField]
		float SubstancePassiveEffectReduction;
		
		public SubstancePassive()
		{
			Name = "Substance Passive";

			BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) => {
			float baseBonusDamage = (owner.Armor - owner.BaseStats.Armor) * SubstancePassiveBonusArmorPercentage;
			float totalBonusDamage = pair.ability.StackValue(baseBonusDamage, SubstancePassiveEffectReduction, owner.StacksOf(pair) - 1);
			float totalBaseDamage = packet.PhysicalDamage + packet.MagicalDamage;
			if (totalBaseDamage > 0) {
				float physicalDamageRatio = packet.PhysicalDamage / totalBaseDamage;
				float magicalDamageRatio = packet.MagicalDamage / totalBaseDamage;
				DamagePacket bonusDamage = new DamagePacket(DamageActionType.ItemEffect, owner, singletarget:packet.HasSingleTarget);
				if (packet.Kind == DamageActionType.SkillOrAbility)
				{
					if (physicalDamageRatio > 0) {
						bonusDamage.PhysicalDamage += totalBonusDamage / physicalDamageRatio;
					}
					if (magicalDamageRatio > 0) {
						bonusDamage.MagicalDamage += totalBonusDamage / magicalDamageRatio;
					}
					owner.DealDamage(target, bonusDamage);
				}
			}
			return packet;
			};
		}
	}
}