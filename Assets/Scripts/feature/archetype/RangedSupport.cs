﻿using core;
using core.forger;

namespace feature.kit
{
    public class RangedSupport : Obsolete_Archetype
    {
        public override UnitStats BaseStats
        {
            get
            {
                return UnitStats.Stats(
                    MaxHealth: 440,
                    HealthRegen: 1,
                    MovementSpeed: 380,
                    AutoAttackDamage: 53,
                    BaseAttackTime: 1.5f,
                    BaseAttackRange: Constants.RANGED_DEFAULT_ATTACK_RANGE
                );
            }
        }

        public override UnitStats PerLevelStats
        {
            get
            {
                // Note: Right now (level - 1) is applied from per level stats. 
                // E.g. At level one, they get BaseStats only. At level two, they get BaseStats + 1 x PerLevelStats. 
                // And At level six, they get BaseStats + 5 x PerLevelStats.

                return UnitStats.Stats(
                    MaxHealth: 65,
                    HealthRegen: .1f,
                    AutoAttackDamage: 2.7f,
                    AttackSpeed: 2.00f
                );
            }
        }

        public override float basicAttackDamagePerPower
        {
            get
            {
                return .3f;
            }
        }

        public override float attackSpeedPerHaste
        {
            get
            {
                return .6f;
            }
        }

        public override float cooldownReductionPerHaste
        {
            get
            {
                return .2f;
            }
        }

    }
}
