﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Conviction", menuName = "Ability/Warcardinal/Conviction")]
    public class ConvictionDebuff : BuffDebuff
    {
        #region conviction variables
        //conviction base variables
        public Unit convictionPreviousUnit = null;
        //conviction debuff variables
        public static BuffDebuff convictionDebuff;
        public float convictionDebuffDuration = 5;
        public float convictionStunDuration = .65f;
        //conviction debuff immunity variables
        public static BuffDebuff convictionImmunityBuff;
        public float convictionImmunityDuration = 8;
        //conviction resource gain variables
        public float convictionResourceGain = 1;
        public float convictionEmpoweredResourceGain = 2;
        #endregion

        public ConvictionDebuff()
        {
            Name = "conviction_debuff";
            MaximumStacks = 3;
            AlsoObsolete_Duration = (pair) => 
            {
                return convictionDebuffDuration;
            };
            //do thing when stack is added - when 3 stacks are added, stun the unit
        }
    }
}