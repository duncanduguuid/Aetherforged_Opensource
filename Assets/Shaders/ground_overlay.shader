﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/ground_overlay" {
	Properties {
		_Color ("Tint color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader 
	{

	Tags { "Queue"="Transparent-15" "RenderType"="Transparent" }

	pass
	{

	Cull Off Lighting off
	ZWrite Off
	colormask RGB
	Blend SrcAlpha OneMinusSrcAlpha
	Alphatest Greater .01

	BindChannels{
		Bind "Color", color
		Bind "Vertex", vertex
		Bind "TexCoord", texcoord
	}

	CGPROGRAM
	#pragma vertex vertexFunction
	#pragma fragment fragmentFunction SimpleSpecular


	#include "UnityCG.cginc"

	//get info from the model
	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv :TEXCOORD0;
		float4 color : COLOR;
	};

	struct v2f
	{
		float4 position : SV_POSITION;
		float2 uv : TEXCOORD0;
		float4 color : COLOR;
	};

	//bring properties
	float4 _Color;
	sampler2D _MainTex;

	//set out the vertices
	v2f vertexFunction(appdata IN)
	{
		v2f OUT;
		OUT.position = UnityObjectToClipPos(IN.vertex);
		OUT.position = UnityObjectToClipPos(IN.vertex);
		OUT.uv = IN.uv;
		OUT.color = IN.color;

		return OUT;
	}

	//color inside vertices
	fixed4 fragmentFunction(v2f IN) : SV_Target
	{
		float4 textureColor = tex2D( _MainTex, IN.uv);
		IN.color.a;
		return (textureColor * _Color * IN.color.a * IN.color);
	}
	ENDCG
	}

	}
}
