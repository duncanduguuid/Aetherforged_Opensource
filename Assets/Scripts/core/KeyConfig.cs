﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace core    
{
    public class KeyConfig : Singleton<KeyConfig> {

        Dictionary<ICE,InputPair> keyChain = new Dictionary<ICE, InputPair> ();

        Dictionary<ICE, AbilityKeyGroup> abilityKeys = new Dictionary<ICE, AbilityKeyGroup>();

        public static KeyCode CancelKey {
            get;
            private
            set;
        }
        public List<ICE> ListAbilityGroupKeys { 
            get {
                List<ICE> result = new List<ICE>();

                result.AddRange(abilityKeys.Keys);
                return result;
            }
        }


        // Use this for initialization
        void Start () {

            CancelKey = KeyCode.Escape;
            /*
             * This was a test of a bug that cropped up when I typoed in this section - Mox
            SetKey(ICE.AbilityOneForger, description: "Ability 1", key: KeyCode.Q);
            SetKey(ICE.MousePanCamera, description: "Mouse Pan", key: KeyCode.Mouse2);
            SetKey(ICE.ToggleWindowShop, description: "Open Shop", key: KeyCode.P);
            */

            GetGroupKey(ICE.AbilityOneForger, "Ability 1", quick: KeyCode.Q, skillup: KeyCode.Q, skillupMods: EventModifiers.Control);
            GetGroupKey(ICE.AbilityTwoForger, "Ability 2", quick: KeyCode.W, skillup: KeyCode.W, skillupMods: EventModifiers.Control);
            GetGroupKey(ICE.AbilityThreeForger, "Ability 3", quick: KeyCode.E, skillup: KeyCode.E, skillupMods: EventModifiers.Control);
            GetGroupKey(ICE.AbilityFourForger, "Ability 4", quick: KeyCode.R, skillup: KeyCode.R, skillupMods: EventModifiers.Control);

            GetGroupKey(ICE.AttackCursorForger, "Attack Move", simple: KeyCode.A);

            GetGroupKey(ICE.RecallForger, "Recall", quick: KeyCode.B);

            /*
            GetKey(ICE.AbilityOneForger, description: "Ability 1", key: KeyCode.Q);
            GetKey(ICE.AbilityTwoForger, description: "Ability 2", key: KeyCode.W);
            GetKey(ICE.AbilityThreeForger, description: "Ability 3", key: KeyCode.E);
            GetKey(ICE.AbilityFourForger, description: "Ability 4", key: KeyCode.R);
            GetKey(ICE.AttackCursorForger, description: "Attack", key: KeyCode.A);
            GetKey(ICE.RecallForger, description: "Recall", key: KeyCode.B);
            */
            GetKey(ICE.StopActionsForger, description: "Stop", key: KeyCode.S);


            GetKey(ICE.CursorConfirmForger, description: "Confirm Action", key: KeyCode.Mouse0);
            GetKey(ICE.MoveActionForger, description: "Move Click", key: KeyCode.Mouse1);
            GetKey(ICE.MousePanCamera, description: "Mouse Pan", key: KeyCode.Mouse2);

            GetKey(ICE.ToggleLockCamera, description: "Camera Lock", key: KeyCode.Y);
            GetKey(ICE.CenterForgerCamera, description: "Center Forger", key: KeyCode.Space);

            GetKey(ICE.ToggleWindowShop, description: "Open Shop", key: KeyCode.P);

            GetKey(ICE.BackOptionMenu, description: "Menu", key: KeyCode.Escape);


        }

        #if UNITY_EDITOR

        [ContextMenu("CheckInstance")]
        public void CheckInstance () {
            // Fixes an issue that happens when the Unity Editor recompiles in play mode. 
            if (keyChain.Count == 0) {
                Start();
            }
        }

        void Update () {
            CheckInstance();
        }
        #endif

        public bool QuickCast(ICE inputControlEnum, Event keyEvent) 
        {
            return abilityKeys.ContainsKey(inputControlEnum)
                              && keyEvent.keyCode == abilityKeys[inputControlEnum].QuickCastKey
                              && keyEvent.modifiers == abilityKeys[inputControlEnum].QuickCastModifiers;
        }

        public bool IndicatorCast(ICE inputControlEnum, Event keyEvent) 
        {
            return abilityKeys.ContainsKey(inputControlEnum)
                              && keyEvent.keyCode == abilityKeys[inputControlEnum].IndicatorCastKey
                              && keyEvent.modifiers == abilityKeys[inputControlEnum].IndicatorCastModifiers;
        }

        public bool SkillUp(ICE inputControlEnum, Event keyEvent) 
        {
            return abilityKeys.ContainsKey(inputControlEnum)
                              && keyEvent.keyCode == abilityKeys[inputControlEnum].QuickSkillUpKey
                              && keyEvent.modifiers == abilityKeys[inputControlEnum].SkillUpModifiers;
        }


        public bool IsPressed(ICE inputControlEnum) {
            return keyChain.ContainsKey(inputControlEnum)
                && keyChain [inputControlEnum].GetKey;
        }

        public bool WentDown(ICE inputControlEnum) {
            return keyChain.ContainsKey(inputControlEnum)
                && keyChain [inputControlEnum].WasDown;
        }

        public bool WentUp(ICE inputControlEnum) {
            return keyChain.ContainsKey(inputControlEnum)
                && keyChain [inputControlEnum].WasUp;
        }

        public string Describe(ICE inputControlEnum) {
            if (abilityKeys.ContainsKey(inputControlEnum))
            {
                return abilityKeys[inputControlEnum].description;
            }
            if (keyChain.ContainsKey(inputControlEnum)) {
                return keyChain[inputControlEnum].description;
            } 

            return "Key not found <DEBUG>";

        }

        public string ShowKey(ICE inputControlEnum, int whichKey = 0) {
            if (abilityKeys.ContainsKey(inputControlEnum))
            {
                return abilityKeys[inputControlEnum].QuickToString;
            }
            else
            if (keyChain.ContainsKey(inputControlEnum)) {
                switch (whichKey) {
                case 0:
                    return keyChain [inputControlEnum].KeyZero.ToString();
                case 1:
                    return keyChain [inputControlEnum].KeyOne.ToString();
                default:
                    Debug.LogErrorFormat("KeyConfig:ShowKey index = {0} isn't accepted", whichKey);
                    return string.Format("KeyConfig:ShowKey index = {0} isn't accepted", whichKey);
                }
            } else {
                return string.Format("KeyConfig:ShowKey Key: {0} is not set yet.", inputControlEnum);
            }
        }

        public string ShowQuickKey(ICE inputControlEnum) {
            if (abilityKeys.ContainsKey(inputControlEnum))
            {
                return abilityKeys[inputControlEnum].QuickToString;
            }
            return "<Not Found>";
        }

        public string ShowSimpleKey(ICE inputControlEnum){
            if (abilityKeys.ContainsKey(inputControlEnum))
            {
                return abilityKeys[inputControlEnum].SimpleToString;
            }
            return "<Not Found>";
        }

        public string ShowLevelUpKey(ICE inputControlEnum)
        {
            if (abilityKeys.ContainsKey(inputControlEnum))
            {
                return abilityKeys[inputControlEnum].SkillUpToString;
            }
            return "<Not Found>";
        }

        public void GetGroupKey(ICE inputControlEnum, string description = ""
         , KeyCode quick = KeyCode.None, EventModifiers quickMods = EventModifiers.None
         , KeyCode simple = KeyCode.None, EventModifiers simpleMods = EventModifiers.None
         , KeyCode skillup = KeyCode.None, EventModifiers skillupMods = EventModifiers.None)
        {
            if (quick != KeyCode.None)
            {
                var quickString = PlayerPrefs.GetString(string.Format("{0}.QuickCast", inputControlEnum), quick.ToString());
                quick = (KeyCode)Enum.Parse(typeof(KeyCode), quickString);
                quickMods = (EventModifiers)PlayerPrefs.GetInt(string.Format("{0}.QuickMods", inputControlEnum), (int)quickMods);
            }

            if (simple != KeyCode.None)
            {
                var simpleString = PlayerPrefs.GetString(string.Format("{0}.SimpleCast", inputControlEnum), simple.ToString());
                simple = (KeyCode)Enum.Parse(typeof(KeyCode), simpleString);
                simpleMods = (EventModifiers)PlayerPrefs.GetInt(string.Format("{0}.SimpleMods", inputControlEnum), (int)simpleMods);
            }

            if (skillup != KeyCode.None)
            {
                var skillupString = PlayerPrefs.GetString(string.Format("{0}.SkillUp", inputControlEnum), skillup.ToString());
                skillup = (KeyCode)Enum.Parse(typeof(KeyCode), skillupString);
                skillupMods = (EventModifiers)PlayerPrefs.GetInt(string.Format("{0}.SkillUpMods", inputControlEnum), (int)skillupMods);
            }

            SetGroupKey(inputControlEnum, description, quick, quickMods, simple, simpleMods, skillup, skillupMods);
        }

        public void GetKey(ICE inputControlEnum, KeyCode key, int whichKey = 0, string description = "")
        {
            string result = PlayerPrefs.GetString(inputControlEnum.ToString(), key.ToString());

            KeyCode keyOrDefault = (KeyCode)System.Enum.Parse(typeof(KeyCode), result);

            SetKey(inputControlEnum, keyOrDefault, whichKey, description);

        }

        public void SaveGroupKey(ICE inputControlEnum, string description = ""
         , KeyCode quick = KeyCode.None, EventModifiers quickMods = EventModifiers.None
         , KeyCode simple = KeyCode.None, EventModifiers simpleMods = EventModifiers.None
         , KeyCode skillup = KeyCode.None, EventModifiers skillupMods = EventModifiers.None)
        {
            if (quick != KeyCode.None)
            {
                PlayerPrefs.SetString(string.Format("{0}.QuickCast", inputControlEnum), quick.ToString()); 
                PlayerPrefs.SetInt(string.Format("{0}.QuickMods", inputControlEnum), (int)quickMods);
            }

            if (simple != KeyCode.None)
            {
                PlayerPrefs.SetString(string.Format("{0}.SimpleCast", inputControlEnum), simple.ToString());
                PlayerPrefs.SetInt(string.Format("{0}.SimpleMods", inputControlEnum), (int)simpleMods);
            }

            if (skillup != KeyCode.None)
            {
                PlayerPrefs.SetString(string.Format("{0}.SkillUp", inputControlEnum), skillup.ToString());
                PlayerPrefs.SetInt(string.Format("{0}.SkillUpMods", inputControlEnum), (int)skillupMods);
            }

            SetGroupKey(inputControlEnum, description, quick, quickMods, simple, simpleMods, skillup, skillupMods);
        }

        public void SaveKey(ICE inputControlEnum, KeyCode key, int whichKey = 0, string description = "")
        {
            PlayerPrefs.SetString(inputControlEnum.ToString(), key.ToString());

            SetKey(inputControlEnum, key, whichKey, description);
        }


        public void SetGroupKey(ICE inputControlEnum, string description = ""
         , KeyCode quick = KeyCode.None, EventModifiers quickMods = EventModifiers.None
         , KeyCode simple = KeyCode.None, EventModifiers simpleMods = EventModifiers.None
         , KeyCode skillup = KeyCode.None, EventModifiers skillupMods = EventModifiers.None)
        {
            if (abilityKeys.ContainsKey(inputControlEnum))
            {
                if (quick != KeyCode.None)
                {
                    abilityKeys[inputControlEnum].QuickCastKey = quick;
                    abilityKeys[inputControlEnum].QuickCastModifiers = quickMods;
                }

                if (simple != KeyCode.None)
                {
                    abilityKeys[inputControlEnum].IndicatorCastKey = simple;
                    abilityKeys[inputControlEnum].IndicatorCastModifiers = simpleMods;
                }

                if (skillup != KeyCode.None)
                {
                    abilityKeys[inputControlEnum].QuickSkillUpKey = skillup;
                    abilityKeys[inputControlEnum].SkillUpModifiers = skillupMods;
                }

                if (description != "")
                {
                    abilityKeys[inputControlEnum].description = description;
                }
            }
            else
            {
                if (description == "")
                {
                    description = inputControlEnum.ToString();
                }
                var keyGroup = new AbilityKeyGroup(
                    description,
                    quick: quick, quickMods: quickMods,
                    skillup: skillup, skillupMods: skillupMods,
                    simple: simple, simpleMods: simpleMods);

                abilityKeys.Add(inputControlEnum, keyGroup);
            }
        }

        public void SetKey(ICE inputControlEnum, KeyCode key, int whichKey = 0, string description = "") {
            if (description == "") {
                description = inputControlEnum.ToString();
            }
            if (keyChain.ContainsKey(inputControlEnum)) {
                switch (whichKey) {
                case 0:
                    keyChain [inputControlEnum].KeyZero = key;
                    break;
                case 1:
                    keyChain [inputControlEnum].KeyOne = key;
                    break;
                default:
                    Debug.LogErrorFormat("KeyConfig:SetKey {0} isn't accepted", whichKey);
                    break;
                }
            } else {
                keyChain.Add(inputControlEnum, new InputPair(description, key));
            }
        }

        public KeyValuePair<ICE,InputPair>[] AsArray () {
            List<KeyValuePair<ICE,InputPair>> result = new List<KeyValuePair<ICE, InputPair>>();

            result.AddRange(keyChain);
            return result.ToArray();
        }

        public ICollection<ICE> GetAlreadyICE () {
            return keyChain.Keys;
        }

        [System.Serializable]
        public class InputPair {
            public string description;
            public KeyCode KeyZero;
            public KeyCode KeyOne;

            public KeyCode Key {
                get { return KeyZero; }
            }

            public InputPair(string description, KeyCode key0 = KeyCode.None, KeyCode key1 = KeyCode.None) { 
                this.description = description; 
                KeyZero = key0; 
                KeyOne = key1;
            } 

            public bool GetKey {
                get { return Input.GetKey(KeyZero) || Input.GetKey(KeyOne); }
            }

            public bool WasDown {
                get { return Input.GetKeyDown(KeyZero) || Input.GetKeyDown(KeyOne); }
            }

            public bool WasUp {
                get { return ((KeyZero == KeyCode.None || Input.GetKeyUp(KeyZero)) && (KeyOne == KeyCode.None || Input.GetKeyUp(KeyOne))); }
            }
        }

        [Serializable]
        public class AbilityKeyGroup {
            public string description;
            public KeyCode IndicatorCastKey;
            public EventModifiers IndicatorCastModifiers = EventModifiers.None;
            public KeyCode QuickCastKey;
            public EventModifiers QuickCastModifiers = EventModifiers.None;
            public KeyCode QuickSkillUpKey;
            public EventModifiers SkillUpModifiers = EventModifiers.None;

            public KeyCode SelfCastQuickKey;
            public EventModifiers SelfQuickModifiers = EventModifiers.None;


            public AbilityKeyGroup(string description
               , KeyCode quick = KeyCode.None
               , EventModifiers quickMods = EventModifiers.None
               , KeyCode simple = KeyCode.None
               , EventModifiers simpleMods = EventModifiers.None
               , KeyCode skillup = KeyCode.None
               , EventModifiers skillupMods = EventModifiers.None)
            {
                
                this.description = description;
                QuickCastKey = quick;
                QuickCastModifiers = quickMods;
                IndicatorCastKey = simple;
                IndicatorCastModifiers = simpleMods;
                QuickSkillUpKey = skillup;
                SkillUpModifiers = skillupMods;
            } 

            public string QuickToString {
                get {
                    if (QuickCastModifiers == EventModifiers.None)
                    {
                        return QuickCastKey.ToString();
                    }
                    return QuickCastModifiers.ToString() + " + " + QuickCastKey.ToString();
                }
            }

            public string SimpleToString {
                get {
                    if (IndicatorCastModifiers == EventModifiers.None)
                    {
                        return IndicatorCastKey.ToString();
                    }
                    return IndicatorCastModifiers.ToString() + " + " + IndicatorCastKey.ToString();
                }
            }

            public string SkillUpToString {
                get {
                    if (SkillUpModifiers == EventModifiers.None)
                    {
                        return QuickSkillUpKey.ToString();
                    }
                    return SkillUpModifiers.ToString() + " + " + QuickSkillUpKey.ToString();
                }
            }
        }

    }


    // input control enumeration
    public enum ICE {
        None,
        // Forger ICE
        AbilityOneForger,
        AbilityTwoForger,
        AbilityThreeForger,
        AbilityFourForger,
        AttackCursorForger,
        CursorConfirmForger,
        PlaceWardForger,
        MoveActionForger,
        StopActionsForger,
        RecallForger,
        // Camera ICE
        ToggleLockCamera,
        CenterForgerCamera,
        MousePanCamera,
        // Shop ICE
        ToggleWindowShop,
        // Menu ICE
        BackOptionMenu,
    }
}
