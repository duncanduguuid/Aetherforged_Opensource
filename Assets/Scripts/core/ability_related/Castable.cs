﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace core {
    public class Castable : Ability {

        public CastType CastType {
            get {
                if (OnNonTargetedCast != null) {
                    return CastType.NonTargeted;
                }
                if (AreaCast != null)
                {
                    return CastType.AreaTargeted;
                }
                if (UnitTargetedCast != null)
                {
                    return CastType.UnitTargeted;
                }
                if (LineTargetCast != null)
                {
                    return CastType.LineTargeted;
                }
                if (ConeTargetCast != null)
                {
                    return CastType.ConeTargeted;
                }
                return CastType.Passive;
            }
        }

        public CastableKind Kind;

        [SerializeField]
        public bool IsUltimate {
            get;
            set;
        }


        /// <summary>
        /// The conditions in which this ability can be cast
        /// Args: Castable self, Unit caster
        /// returns true if castable
        /// </summary>
        public Func<Castable, Unit, bool> CastingConditions;

        /// <summary>
        /// The conditions in which this ability can be cast on target
        /// Args: Castable ability, Unit caster, Unit target
        /// returns true if castable on the target Unit.
        /// </summary>
        public Func<Castable, Unit, Unit, bool> TargetedCastingConditions;

        /// <summary>
        /// When this ability is cast on target unit.
        /// Args: Castable self, Unit caster, Unit target.
        /// </summary>
        public Action<Castable, Unit, Unit> UnitTargetedCast;

        /// <summary>
        /// When this ability is cast without selecting a target.
        /// Args: Castable self, Unit caster.
        /// </summary>
        public Action<Castable, Unit> OnNonTargetedCast;

        /// <summary>
        /// When this ability is cast on a selecting a target.
        /// Args: Castable self, Unit caster, Vector3 area.
        /// </summary>
        public Action<Castable, Unit, Vector2> AreaCast;

        /// <summary>
        /// When this ability is cast on a target line area.
        /// Args: Castable self, Unit caster, float width, float length.
        /// </summary>
        public Action<Castable, Unit, Vector2> LineTargetCast;

        /// <summary>
        /// When this ability allows for recasting.
        /// Args: Castable self, Unit caster, float window.
        /// </summary>


        /// <summary>
        /// When this ability is cast on a target cone area.
        /// Args: Castable self, Unit caster, float arc, float length.
        /// </summary>
        public Action<Castable, Unit> ConeTargetCast;


        public bool CanCast(Unit source) {
            return CastingConditions == null || CastingConditions(this, source);
        }

        public bool CanCastOnUnitTarget(Unit source, Unit target) {
            return target != null && (TargetedCastingConditions == null || TargetedCastingConditions(this, source, target));
        }

        /// <summary>
        /// Determines whether this castable ability is on cooldown for the specified caster.
        /// </summary>
        /// <returns><c>true</c> if this ability is on cooldown for the specified caster; otherwise, <c>false</c>.</returns>
        /// <param name="caster">Caster.</param>
        new public virtual bool IsOnCooldown(Unit caster)
        {
            return caster.SkillOnCooldown(this);
        }

        /* public static List<Unit> GetUnitsInLine(Vector3 posA, Vector3 posB, float width, float length)
		{
			List<Unit> inMe = new List<Unit>();

			Vector3 orig = new Vector3();
			orig.x = posA.x;
			orig.y = .5f;
			orig.z = posA.z;

			Vector3 end = new Vector3();
			end.x = posB.x;
			end.y = .5f;
			end.z = posB.z;

			Vector3 dir = (end-orig).normalized;

			RaycastHit[] hits = Physics.BoxCastAll(orig, new Vector3((width / 2), (length / 2), .5f), dir, Quaternion.identity, length, 8);

            foreach(RaycastHit hit in hits)
			{
				GameObject gob = hit.collider.gameObject;
				Unit currUnit = gob.GetComponent<Unit>();
				if(currUnit != null)
				{
					inMe.Add(currUnit);
				}
			}

            Debug.Log("Found " + inMe.Count.ToString() + " units \n");
            Debug.Log("Units found: \n");
            foreach(Unit unit in inMe)
            {
                Debug.Log(unit.name + "\n");
            }
			return inMe;
		}

        public static List<Unit> GetUnitsInLine(GameObject posA, Vector3 posB, float width, float length)
        {
            List<Unit> inMe = new List<Unit>();

            Vector3 orig = new Vector3();
            orig.x = posA.transform.position.x;
            orig.y = .5f;
            orig.z = posA.transform.position.z;

            Vector3 end = new Vector3();
            end.x = posB.x;
            end.y = .5f;
            end.z = posB.z;

            Vector3 dir = (end-orig).normalized;

            RaycastHit[] hits = Physics.BoxCastAll(orig, new Vector3((width / 2), (length / 2), .5f), dir, posA.transform.rotation, length, 8);

            foreach(RaycastHit hit in hits)
			{
				GameObject gob = hit.collider.gameObject;
				Unit currUnit = gob.GetComponent<Unit>();
				if(currUnit != null)
				{
					inMe.Add(currUnit);
				}
			}
            

            Debug.Log("Found " + inMe.Count.ToString() + " units \n");
            Debug.Log("Units found: \n");
            foreach(Unit unit in inMe)
            {
                Debug.Log(unit.name + "\n");
            }

            return inMe;
        } */

        /* public static List<Unit> GetUnitsInCone(Vector3 posA, Vector3 posB, float arc, float length)
		{
			List<Unit> inMe = new List<Unit>();

			Vector3 orig = new Vector3();
			orig.x = posA.x;
			orig.y = .5f;
			orig.z = posA.z;

			Vector3 target = new Vector3();
			target.x = posB.x;
			target.y = .5f;
			target.z = posB.z;

			Vector3 dir = (target-orig).normalized;

			Collider[] allHit = Physics.OverlapSphere(orig, length, 8);
			foreach(Collider col in allHit)
			{
				float angle = Vector3.Angle(dir, (col.transform.position - orig));
                if(angle < 0) 
                {
                    angle = -angle;
                }
				if(angle <= (arc/2))
				{				
					GameObject gob = col.gameObject;
					Unit currUnit = gob.GetComponent<Unit>();
					if(currUnit != null)
					{
						inMe.Add(currUnit);
					}
				}
			}

            Debug.Log("Found " + inMe.Count.ToString() + " units \n");
            Debug.Log("Units found: \n");
            foreach(Unit unit in inMe)
            {
                Debug.Log(unit.name + "\n");
            }
			return inMe;
		}
 */

    }
    public class CastType {
        public static CastType NonTargeted = new CastType();
        public static CastType AreaTargeted = new CastType();
        public static CastType LineTargeted = new CastType();
        public static CastType ConeTargeted = new CastType();
        public static CastType Passive = new CastType();
        internal static CastType UnitTargeted = new CastType();
    }
    public class CastableKind {
        public readonly static CastableKind Basic = new CastableKind();
        public readonly static CastableKind Ultimate = new CastableKind();
        public readonly static CastableKind Spell = new CastableKind();
    }
}
