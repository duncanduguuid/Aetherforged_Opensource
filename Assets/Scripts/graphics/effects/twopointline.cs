﻿using UnityEngine;
using System.Collections;

public class twopointline : MonoBehaviour {

	private LineRenderer line_renderer;
	public Transform start_point;
	public Transform end_point;

	// Use this for initialization
	void Start () {
		line_renderer = GetComponent<LineRenderer>();

		if (start_point == null) 
		{
			start_point = gameObject.transform;
		} 
		else 
		{
			line_renderer.SetPosition (0, start_point.position);
		}

		if (end_point == null) 
		{
			end_point = start_point;
		} 
		else 
		{
			line_renderer.SetPosition (1, end_point.position);
		}

	
	}
	
	// Update is called once per frame
	void Update () {

		line_renderer.SetPosition (0, start_point.position);
		line_renderer.SetPosition (1, end_point.position);
		float distance = Vector3.Distance (start_point.position, end_point.position);
		line_renderer.material.SetTextureScale ("_MainTex", new Vector2(distance ,1));
	}
}
