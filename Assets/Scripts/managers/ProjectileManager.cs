﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using controllers;
using core;

namespace managers{

    public class ProjectileManager : Singleton<ProjectileManager> {
        
        [SerializeField]
        List<ProjectileDressing> register;

        [SerializeField]
        GameObject defaultDressing;

        [SerializeField]
        ProjectileController bareProjectile;

        Dictionary<string, GameObject> dressings = new Dictionary<string, GameObject>();


        void Awake () {
            instance = this;
            foreach (ProjectileDressing dressing in register) {
                dressings.Add(dressing.Name, dressing.DressingPrefab);
            }
        }

        public void Register(string which, GameObject prefab) {
            if (!dressings.ContainsKey(which) && prefab != null) {
                dressings.Add(which, prefab);
            }
        }

        public void ApplyDressing(string which, Transform projectile) {
            GameObject dressing;
            if (dressings.ContainsKey(which)) {
                dressing = Instantiate(dressings [which], projectile);
            } else {
                dressing = Instantiate(defaultDressing, projectile);
            }
            // TODO 2017 July 18 Add a height offset
            dressing.transform.localPosition = Vector3.up;
        }

        public ProjectileController Gimme(string which, Unit target, Unit source, float speed = 200
            , float radius = 15
            , Vector3 startOffset = default(Vector3)
            , Action<ProjectileController> OnStop = null
            , Action<ProjectileController, Unit> OnHitUnit = null
            , Action<ProjectileController, Unit, Unit> OnHitTarget = null
            , bool BreakWhenTargetLost = true
        ) {

            var pico = commonGimme(which);

            pico.Target = target;
            pico.Velocity = speed;
            pico.Source = source;
            pico.Radius = radius;
            pico.StopWhenTargetLost = BreakWhenTargetLost;

                #if UNITY_EDITOR
            pico.transform.SetParent(GameManager.Instance.projectileRoot);
                #endif
            if (source != null)
            {
                if (source.attachPoint != null)
                {
                    pico.transform.position = source.attachPoint.position + startOffset;
                }
                else
                {
                    pico.transform.position = source.transform.position + startOffset;
                }

                pico.OnHitTarget = OnHitTarget;
                pico.OnHitUnit = OnHitUnit;
                pico.OnStop = OnStop;

                NetworkServer.Spawn(pico.gameObject);
            }
            return pico;
        }

        public ProjectileController Gimme(string which, Vector2 target, Unit source, float speed = 200
            , float radius = 15
            , Vector3 startOffset = default(Vector3)
            , Action<ProjectileController> OnStop = null
            , Action<ProjectileController, Unit> OnHitUnit = null
        ) {

            var pico = commonGimme(which);
            pico.dressing = which;
            pico.SetPlanar(target);
            pico.Velocity = speed;
            pico.Source = source;
            pico.Radius = radius;

            #if UNITY_EDITOR
            pico.transform.SetParent(GameManager.Instance.projectileRoot);
            #endif
            pico.transform.position = source.transform.position + startOffset;
            pico.OnHitUnit = OnHitUnit;
            pico.OnStop = OnStop;

            NetworkServer.Spawn(pico.gameObject);

            return pico;
        }

        private TargetedProjectileController commonGimme (string which) {
            string dressingName; 

            if (dressings.ContainsKey(which)) {
                // TODO 2017 Jul 18 What do we want to do if we know it won't find a dressing?

            }

            dressingName = which;

            var pcob = GameObject.Instantiate(bareProjectile);
            pcob.dressing = dressingName;

            return pcob.GetComponent<TargetedProjectileController>();
        }

        public bool CleanUp(ProjectileController projectile) {
            // TODO 

            NetworkServer.Destroy(projectile.gameObject);
            return true;
        }


    }
}