﻿using UnityEngine;
using core;

namespace feature
{
	[CreateAssetMenu(fileName = "Sympathy Passive", menuName = "Passives/Items/Sympathy")]
	public class SympathyPassive : Passive
	{
		[SerializeField]
		BuffDebuff SympathyBuff;
		[SerializeField]
		float sympathyRange;

		public SympathyPassive()
		{
			Name = "Sympathy Passive";

			OnTick = (SourcePair pair, Unit owner, PassiveTracker data, float deltaTimer) =>
			{
				Unit lowestHealthAllyForger = null;
				
				Unit.ForEachAllyInRadius(sympathyRange, owner.Position, owner.Team, (Unit ally) =>
				{
					if(ally.Kind == UnitKind.Forger && ally != owner)
						if(lowestHealthAllyForger == null)
						{
							lowestHealthAllyForger = ally;
							lowestHealthAllyForger.AddBuff(SympathyBuff.WithSource(pair.Source));
						}
				});
			};
		}
	}
}