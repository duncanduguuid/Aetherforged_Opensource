﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace core {
    public class BuffDebuff : Passive {

        [SerializeField]
        protected float duration;
        public float Duration
        {
            get { return duration; }
        }

        public bool AffectedByTenacity;

        [Obsolete]
        public Func<SourcePair, float> AlsoObsolete_Duration;

        /// <summary>
        /// When stacking this buff, do not refresh the timer. 
        /// Allow each timer to run out in their own time. 
        /// </summary>
        public bool DoNotRefreshOnStack = false;

        /// <summary>
        /// When this buff falls off, remove it completely.
        /// </summary>
        public bool DropOffAltogether = false;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:core.BuffDebuff"/> can be cleansed.
        /// </summary>
        /// <value><c>true</c> if can be cleansed; otherwise, <c>false</c>.</value>
        public bool CanBeCleansed { get; set; }

        [Obsolete]
        public Func<Passive, float> Obsolete_Duration;

        public virtual Color OutlineColor
        {
            get
            {
                return Color.cyan;
            }
        }

        public virtual bool HideOnBuffBar {
            get {
                return false;
            }
        }

    }

    public class BuffTracker {
        public Timer timer;
        public bool HasDuration {
            get {
                return (timer != null);
            }
        }

        /// <summary>
        /// The timestamp when we applied this passive.
        /// </summary>
        public float appliedTime;



        public float timeSinceApplication
        {
            get
            {
                return GameTime.time - appliedTime;
            }
        }

    }
}