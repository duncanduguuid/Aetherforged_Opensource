﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Tower Pen Attack Buff", menuName = "Ability/Lane/Tower Pen Attack Buff")]
    public class TowerShredBuff : BuffDebuff
    {
        [SerializeField]
        float buffDuration = 8f;
        public TowerShredBuff()
        {
            Name = "I'm 60% a scrap shredder";
            DynamicPercentileStats = (pair, holder, data) => 
            {
                return UnitStats.Stats(
                    Penetration: data.stacks * 5f
                );
            };
            AlsoObsolete_Duration = (arg) => 
            {
                return buffDuration;
            };

            MaximumStacks = 4;
        }
    }
}