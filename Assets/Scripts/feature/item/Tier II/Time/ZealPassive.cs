﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;


namespace feature
{
	[CreateAssetMenu(fileName = "Zeal Passive", menuName = "Passives/Items/Zeal")]
	public class ZealPassive : Passive
	{
		[SerializeField]
		BuffDebuff ZealDebuff;
		public ZealPassive()
		{
			BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
			{
				if(target.Kind == UnitKind.Forger && target.Team != owner.Team)
				{
					target.AddBuff(ZealDebuff.WithSource(pair.Source));
				}
				return packet;
			};
		}
	}
}