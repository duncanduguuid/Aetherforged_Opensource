﻿using UnityEngine;
using core;

namespace feature
{
	[CreateAssetMenu(fileName = "Zeal Debuff", menuName = "Passives/Items/ZealDebuff")]
	public class ZealDebuff : BuffDebuff
	{
		[SerializeField]
		BuffDebuff ZealBuff;
		[SerializeField]
		BuffDebuff ZealImmunityDebuff;
		[SerializeField]
		BuffDebuff ZealDebuff2;
		public ZealDebuff()
		{
			Name = "Zeal Debuff";

			AfterTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, Unit source) =>
			{
				if(source.Kind == UnitKind.Forger && source.Team != owner.Team && source != pair.Source && !source.HasBuff(ZealImmunityDebuff.WithSource(pair.Source)))
				{
					source.AddBuff(ZealBuff.WithSource(pair.Source));
					source.AddBuff(ZealImmunityDebuff.WithSource(pair.Source));
					owner.RemoveBuff(ZealDebuff2.WithSource(pair.Source), fullClear:true);
				}
			};
		}
	}
}